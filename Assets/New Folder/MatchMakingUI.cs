﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class MatchMakingUI : MonoBehaviour
{
    public GameObject[] Opponents = new GameObject[4];
    public Text[] oppName = new Text[4];
    public Image[] oppImg = new Image[4];
    public GameObject rotObj, matcher, echor;
    public Image fadeImg;
    public Sprite testSprite;


    int oppCount=-1;

    public void Addplayer()
    {
        string name = Random.Range(0,100).ToString() + "player "; 
        AddPlayer(name,null);
    }

    //public UnityEngine.SceneManagement. scene;
    //string[] oppName = new string[4];
    //Sprite[] oppImg = new Sprite[4];


    Vector3 rotEuler = new Vector3(0, 0, 0);
   
    //struct Opponent
    //{
    //    int no;
    //    string name;
    //    Sprite image;
    //}
    //Opponent[] opponents = new Opponent[4];
    void Start()
    {
        //scene.buildIndex 
        tempCount = oppCount;
      //  StartCoroutine(MatchRotate(1.1f));

      StartMatching();

        
    }

    public enum PlayerType { twoP, fiveP}
    //public void StartMatching(PlayerType ptype)
    //{
    //    if(ptype == PlayerType.fiveP)
    //}
    //public enum PlayerType { twoP, fiveP }
    public void StartMatching()
    {
        // if (ptype == PlayerType.fiveP)
        StartCoroutine(MatchRotate(1.1f));
    }

    public void AddPlayer(string name, Sprite sp)
    {
        PlayerJoined();

        oppName[oppCount].text = name;
        oppImg[oppCount].sprite = sp;
        oppImg[oppCount].gameObject.SetActive(true);
    }

    public void matchPlayer()
    {

        isMatching = false;
        StartCoroutine(PlayerMatch());
    }

    // Update is called once per frame

    float timer = 0;
    bool timerReached = true;


    public bool rotating = true;

    float acceleration = 60f;
    float maxAngle = 360f;
    float angle = 360f;
    float tangle;
    float timeBwSpawns=0.1f, startBwSpawns;
  

   

    float duration = 1.5f;


    bool isMatching = true, isOverMatching, cyComplete;
    bool matchFound;
    


    int tempCount;
    IEnumerator MatchRotate(float duration)
    {
        
        Quaternion startRot = rotObj.transform.rotation;
        float t = 0.0f;
        cyComplete = false;
        while (t < duration)
        {
            t += Time.deltaTime;
            rotObj.transform.rotation = startRot * Quaternion.AngleAxis(t / duration * 360f, Vector3.forward);
           // Debug.Log(rotObj.transform.rotation);
            matcher.transform.position = rotObj.transform.GetChild(0).position;
            if(matchFound)
            {

                if (oppCount > tempCount)
                {
                   // Debug.Log(rotObj.transform.eulerAngles.z);
                    if (rotObj.transform.eulerAngles.z > lowAngle && rotObj.transform.eulerAngles.z < upAngle)
                    {
                        tempCount += 1;
                        Opponents[tempCount].SetActive(true);
                        lowAngle += 90; upAngle += 90;
                        //matchFound = false;
                        Debug.Log(1);
                        StartCoroutine(FadeImage(Color.white, 0.25f, 0f, 1f));
                        //yield return 1;
                    }
                }
                else
                    matchFound = false;
                
            }

            yield return null;
        }
        
       // yield return new WaitForSeconds(0.2f);
        rotObj.transform.rotation = startRot;
        cyComplete = true;
        if (isMatching && !isOverMatching)
        {
            StartCoroutine(MatchRotate(duration));
        }
        //if (isOverMatching)
        //    StartCoroutine(PlayerMatch());
    }

   


    int lowAngle = 87, upAngle = 92;
    //[MenuItem("Window/Edit Mode Functions")]
    //string[] testnames = { "Deysaappan", "Konakaran", "Sheshii", "Pishku" };
    void PlayerJoined()
    {
        if (oppCount >= 3)
        {
            //isOverMatching = true;
            StartCoroutine(PlayerMatch());
            matcher.SetActive(false);
            return;
        }
            
        oppCount++;

        matchFound = true;
        //rotEuler += new Vector3(0f, 0f, 90f);

        //lowAngle += 90; upAngle += 90;
        // if(test >= 360)
        //StartCoroutine(RotateMatch(opp1, transform.position, Vector3.forward, 90, 0.4f));
        //AddPlayer(testnames[oppCount], testSprite);

    }

    
    
    
    
    IEnumerator PlayerMatch()
    {
         
        while (cyComplete != true)
        {
            
            yield return null;
            
        }
        //isMatching = false;
        matcher.gameObject.SetActive(false);
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        List<int> oppTrans = new List<int>();
        if(oppCount == 0)
            Debug.Log("2P");
        float currentTime = 0.0f, inTimeSecs =0.8f;
        // float angleDelta = 45 / inTimeSecs;
        float angleDelta;
        float ourTimeDelta = 0;

        switch (oppCount)
        {
            case 0: //oppTrans.Add(0);
                    
                    break;
            case 1:
                oppTrans.Add(1);
                Opponents[1].transform.GetComponentInChildren<Slider>().transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90));
                angle = 90;
                //Opponents[0]
                break;
            case 2:
                oppTrans.Add(1); oppTrans.Add(2);
                Opponents[1].transform.GetComponentInChildren<Slider>().transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 45));
                Opponents[2].transform.GetComponentInChildren<Slider>().transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 45));
                angle = 45;
                break;
            case 3:
                for (int i = 0; i <= oppCount; i++)
                {
                    oppTrans.Add(i); 
                    Opponents[i].transform.GetComponentInChildren<Slider>().transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 45));
                 
                }
                
                angle = 45;            
                
                break;
        }
        Debug.Log(angle + "- Angle");
        angleDelta = angle / inTimeSecs;
        
        while (currentTime < inTimeSecs)
        {
            currentTime += Time.deltaTime;
            ourTimeDelta = Time.deltaTime;

            if (currentTime > inTimeSecs)
                ourTimeDelta -= (currentTime - inTimeSecs);
            foreach( int i in oppTrans)
            {
                Opponents[i].transform.RotateAround(rotObj.transform.position, Vector3.forward, angleDelta * ourTimeDelta);
                Opponents[i].transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                //Opponents[i].transform.GetComponentInChildren<Slider>().transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, -angle));
                //Opponents[i].transform.GetComponentInChildren<Slider>().value = Mathf.Lerp(1f, 10f, currentTime/ inTimeSecs);
               
            }

            

                //for (int i = 0; i <= oppCount; i++)
                //{
                //    Opponents[i].transform.RotateAround(rotObj.transform.position, Vector3.forward, angleDelta * ourTimeDelta);
                //    Opponents[i].transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                //}
                yield return null;
        }
        StartCoroutine(FadeImage(Color.black, 5, 0.3f, 0.6f));
        fadeImg.gameObject.transform.GetChild(0).transform.gameObject.SetActive(false);
        currentTime = 0f;
        inTimeSecs = 0.5f;
        while (currentTime < inTimeSecs)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i <= oppCount; i++)
            {
                Opponents[i].transform.GetComponentInChildren<Slider>().value = Mathf.Lerp(1f, 10f, currentTime / inTimeSecs);
                //Opponents[i].transform.GetComponentInChildren<Slider>().gameObject.transform.GetChild(0).GetComponent<Image>().color.a = 0.5f
                Opponents[i].transform.localScale = Vector3.Lerp(Opponents[i].transform.localScale, new Vector3(1.15f, 1.15f,1f), (currentTime / inTimeSecs));
            }
            yield return null;
        }

        Quaternion startRot = rotObj.transform.rotation;

        yield return null;
    }

    IEnumerator FadeImage(Color colour, float secondsForOneFlash, float minAlpha, float maxAlpha)
    {

        fadeImg.color = colour;
        float fadinDur = secondsForOneFlash / 2;
        float fadoutDur = secondsForOneFlash / 2;

       
        for (float t = 0f; t <= fadinDur; t += Time.deltaTime)
        {
            Color newColor = fadeImg.color;
            newColor.a = Mathf.Lerp(minAlpha, maxAlpha, t / fadinDur);
            fadeImg.color = newColor;
            yield return null;
        }
       
        for (float t = 0f; t <= fadoutDur; t += Time.deltaTime)
        {
            Color newColor = fadeImg.color;
            newColor.a = Mathf.Lerp(maxAlpha, minAlpha, t / fadoutDur);
            fadeImg.color = newColor;
            yield return null;
        }

       
    }

}
