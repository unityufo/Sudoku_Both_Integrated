﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEngine.UI;
using System;

public class GameListManager : MonoBehaviour
{

[SerializeField] GameObject gameIconPrefab;
[SerializeField] GameObject GamelistViewContainer;

[SerializeField] GameObject GameTemplateListPanel;


private TemplateSelector templateSelector;


private void Start() {
    templateSelector = this.GetComponent<TemplateSelector>();
    StartCoroutine(GetGamesList());
}
IEnumerator GetGamesList()
        {
            Debug.Log(PlayerPrefs.GetString("token" + Application.dataPath));
            string url = "https://ufogames.webc.in/api/games" ;
            WWWForm form = new WWWForm();
          
            
           
        
         using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("Accept","application/json");
             www.SetRequestHeader("Authorization","Bearer " + UFO_TEMP_GAME_SETTINGS.Token );
            var i = www.SendWebRequest();
      
            while(!i.isDone)
            {       

                    Debug.Log("Waiting :  addTime maybe?" );
               
                 yield return null;
            }
            if (www.isNetworkError || www.isHttpError)
            {
                    Debug.Log(www.downloadHandler.text);
                    

            }

            else
            {

                Debug.Log(www.downloadHandler.text);
                GameListData myDeserializedClass = new GameListData();
                myDeserializedClass = JsonConvert.DeserializeObject<GameListData>(www.downloadHandler.text);
                    

                
        
                
                foreach (var item in myDeserializedClass.data)
                {


                    GameObject gameIcon = GameObject.Instantiate(gameIconPrefab);
                    
                    StartCoroutine(GetPlayerImage(item.image_thumbnail_url,gameIcon.GetComponent<Image>()));
                    gameIcon.GetComponentInChildren<Text>().text = item.name;
                   
                    
                    gameIcon.GetComponent<Button>().onClick.AddListener(()=> { UFO_TEMP_GAME_SETTINGS.GameKey = item.key ; templateSelector.HideorShow(true); });

                   if(!gameIcon.activeInHierarchy) gameIcon.SetActive(true);

                    gameIcon.transform.SetParent(GamelistViewContainer.transform,false);

                    
                    
                }
            
            
            }


        }
        }




     private IEnumerator GetPlayerImage(string url,Image gameIcon)
         {
            Debug.Log(gameIcon);
             UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
             yield return www.SendWebRequest();
     
             Texture2D myTexture = DownloadHandlerTexture.GetContent(www);
     
             Rect rec = new Rect(0, 0, myTexture.width, myTexture.height);
             Sprite spriteToUse = Sprite.Create(myTexture, rec, new Vector2(0.5f, 0.5f), 100);
     
             gameIcon.sprite = spriteToUse;
         }




    public class GameData
    {
        public int id;
        public string key;
        public string name;
        public int is_active;
        public int position;
        public string image_url;
        public string image_thumbnail_url;
    }

    public class GameListData
    {
        public List<GameData> data;
        public string message;
        public object errorCode;
        public List<object> errors;
    }





}
