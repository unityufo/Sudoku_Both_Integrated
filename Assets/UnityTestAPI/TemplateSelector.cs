using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;

using System;
using Newtonsoft.Json;

public class TemplateSelector : MonoBehaviour
{
   
    public Transform   TemplatePanel;
    [SerializeField] Transform  contentViewer;
    [SerializeField] List<Transform> contentsTransformList = new List<Transform>(); // the child objects in the contentViewer gameObject
    [SerializeField] GameObject templateButtonPrefab;

    [SerializeField] Text message;

    
    
    
    private void Start() 
    {
        //contentsTransform.Add(templateButtonPrefab.transform);
        TemplatePanel.gameObject.SetActive(false);
    }
    
    
    
    
    public void HideorShow(bool val)
    {
        Debug.Log("Hide or Show called");
        //RefreshPanel(null);  //passingg null will  will force to fetch templatelist from server
        DeactivateAllContents();
        TemplatePanel.gameObject.SetActive(val);   
        GetTemplateList();
        
    }

    
    
    
    public void DeactivateAllContents()
    {
        foreach (var item in contentsTransformList)
        {
            item.gameObject.SetActive(false);
        }

        message.text = "";
    }

    
    
    
    void SetMessage(string msg)
    {
        message.text = msg;
    }
   
   
   
    public void RefreshPanel(List<Contest.Table> contestTables)
    {
        
        DeactivateAllContents();


        if(contestTables == null || contestTables.Count == 0)
        {   
            Debug.LogError("Contest List _ListEmpty");
            SetMessage("No contest data found for " + UFO_TEMP_GAME_SETTINGS.GameKey);
           // GetTemplateList();
            return;
        }
        else
        {
            int i =0;
            foreach (var item in contestTables)
            {
                if(i <= contentsTransformList.Count-1)
                {
                    contentsTransformList[i].gameObject.SetActive(true);
                }
                else
                {
                    SpawnAndSetTemplateContent(item);
                }

                i++;
            }
        }
        

        
    }


    void SetTemplateContent(Transform contestContentObject,Contest.Table contestContentTable)
    {

    }


    void SpawnAndSetTemplateContent(Contest.Table contestContentTable)
    {
        GameObject contestContentObject = SpwanTemplateContent();
        contentsTransformList.Add(contestContentObject.transform);
    }

    private GameObject SpwanTemplateContent()
    {
        GameObject contestCon = Instantiate(templateButtonPrefab);
        contestCon.transform.SetParent(contentViewer, false);
        contestCon.SetActive(true);
        return contestCon;
    }

    public void GetTemplateList()
    {
       // GameSettingsData.SetSelectedGame(_selecteddGame);
        StartCoroutine(GetTemplateListFromServer());
    }


         IEnumerator GetTemplateListFromServer()
        {
           

            Debug.Log(PlayerPrefs.GetString("token" + Application.dataPath));
            string url = "https://ufogames.webc.in/api/ufo/match-list" ;
            WWWForm form = new WWWForm();
            form.AddField("game_key",UFO_TEMP_GAME_SETTINGS.GameKey);
            
           
        
            using (UnityWebRequest www = UnityWebRequest.Post(url,form))
            {
                www.SetRequestHeader("Accept","application/json");
                www.SetRequestHeader("Authorization","Bearer " + UFO_TEMP_GAME_SETTINGS.Token);
                var request = www.SendWebRequest();
        
                while(!request.isDone)
                {       
                    SetMessage("fetching from server ... " + request.progress);
                    yield return null;
                }


                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.downloadHandler.text);
                     SetMessage(www.error);
                 
                }

                else
                {

                    Debug.Log(www.downloadHandler.text);

                    Contest.ContestData myContestData = new Contest.ContestData();
                    myContestData = JsonConvert.DeserializeObject<Contest.ContestData>(www.downloadHandler.text);
                      

            
                    
                    if(myContestData.data.Count != 0) //just to avoid deserialisation null exeptions
                    {   
                        List<Contest.Table> tables = myContestData.data[0].tables;

                        if(tables != null) RefreshPanel(tables); else RefreshPanel(null);

                    }
                    else
                    {
                        RefreshPanel(null);
                    }
                  
                }


            }
        }

        

        public void SetCurrentGameTemplate(int num)
        {
           
            UFO_TEMP_GAME_SETTINGS.Template_id = num.ToString();
        }

public class Contest // class to deal with contestDetails recieved from server (json)
{
     public class Datum
    {
        public int group;
        public List<Table> tables;
    }

    public class PrizeDistribution
    {
        public string rank_from;
        public string rank_to;
        public string prize_type;
        public string prize;
        public string total_distribution_min;
        public string total_distribution_max;
    }

    public class ContestData
    {
        public List<Datum> data;
        public string message;
        public object errorCode;
        public List<object> errors;

      
    }

    public class Table
    {
        public int id;
        public string name;
        public int max_players;
        public string entry_fee;
        public List<PrizeDistribution> prize_distribution;
        public int users_count;
        public int total_winners;
        public string total_prize_money;
    }


}

   

}
