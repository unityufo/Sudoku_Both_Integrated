﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using SimpleJSON;
using UnityEngine.Networking;
using UnityEngine.UI;

using Newtonsoft.Json;




namespace UFO.GlobalComponents
{
    public class GlobalGameManager : MonoBehaviour
{
    static AndroidJavaObject overrideActivity;
    public static GlobalGameManager _instance ;

    public bool createdRoom;

    [SerializeField]Image  playerProfile ;
    public Image PlayerProfile { get => playerProfile; set => playerProfile = value; }

    [SerializeField] GameConfigList gameConfigList;

    GameConfigData currentSelectedGameData;

    [SerializeField] Image backgroundImage;
    public bool CheckifGameExists(string gameKey)
    {
        GameConfigData configData= gameConfigList.GetGameConfigData(gameKey);
        
        if(configData != null) return true;
        else return false;
    }

    public void SetCurrentSelectedGame(string gameKey)
    {
        currentSelectedGameData = gameConfigList.GetGameConfigData(gameKey);
        ///bugs here

        backgroundImage.sprite = currentSelectedGameData.GameBackGroundImage;

    }
    public GameConfigData GetSelectedGame()
    {
        return currentSelectedGameData;
    }
      void Awake()
      {
         if (_instance == null){

            _instance = this;
            DontDestroyOnLoad(this);  
        } 
        else
        {
            Destroy(this);
        }
        
        matchmaker = FindObjectOfType<MatchmakingSetup>();

      }

    MatchmakingSetup matchmaker;
    private void Start() {

        // var i = new { hideFlags= "" , hh = " " , j = new { contest = "" , dat = new { }     } };

        
        // string jsion = JsonConvert.SerializeObject(i);
        

        // Debug.Log(jsion);


       

        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.company.product.OverrideUnityActivity");
            AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
            overrideActivity.Call("OnUnityLoaded");
        }
         catch(Exception e)
        {
           Debug.Log("lol error"+ e);
        }
    }








     void RunPreUnloadFunctions()  //vcall from android 
    {
       StartCoroutine(PreUnloadFunctions());
    }



    // public void TestCall()
    // {
    //      try
    //     {
    //     AndroidJavaClass jc = new AndroidJavaClass("com.company.product.OverrideUnityActivity");
    //         AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");    
    //     overrideActivity.Call("Testcall");//>>>>>>>>>>>>>>error
    //     }
    //     catch(Exception e)
    //     {
    //         Debug.LogError("TestCallFailed"+ e);
    //     }
    // }

    public void ForceUnloadUnity()
    {
        Application.Unload();
    }

    public void ForceQuit()
    {
        Application.Quit();
    }



    IEnumerator PreUnloadFunctions()
    {
        yield return UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);

        yield return null;

        matchmaker = FindObjectOfType<MatchmakingSetup>();

        overrideActivity.Call("OnPreUnloadFunctionsExecuted");
    }


    public void SendGameCompletedCall()
    {
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.company.product.OverrideUnityActivity");
            AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
            overrideActivity.Call("OnGameCompleted");
            
        }
        catch(Exception e)
        {
            Debug.LogError("SendCompleted Failed" + e);
            
        }
    }





//  void  Update()
// {
//     if(Input.GetKeyDown(KeyCode.L)) SendDataAndStartGameAuto(data);
//     else if(Input.GetKeyDown(KeyCode.M)) SendDataAndStartGameManual(data);
// }


     void  SendDataAndStartGameAuto(string gameData)
   {
            JSONNode jsondata = JSON.Parse(gameData);

            UFO_TEMP_GAME_SETTINGS.Token = jsondata["token"];  
            UFO_TEMP_GAME_SETTINGS.Template_id = jsondata["templateID"];  
            UFO_TEMP_GAME_SETTINGS.UserName = jsondata["userName"];
            UFO_TEMP_GAME_SETTINGS.GameKey = jsondata["gameKey"];
            SetCurrentSelectedGame(UFO_TEMP_GAME_SETTINGS.GameKey);
            String url = jsondata["profilePicLink"];

         //   StartCoroutine(GetPlayerImageLocal(url,this.PlayerProfile));

           if(matchmaker != null) matchmaker.forceConnectPhoton();

     
   }

     IEnumerator GetPlayerImageLocal(string url,Image gameIcon)//change the flow structure....weird shit
    {
        if(url == null) yield break;
        Debug.Log(gameIcon);
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        Texture2D myTexture = DownloadHandlerTexture.GetContent(www);

        Rect rec = new Rect(0, 0, myTexture.width, myTexture.height);
        Sprite spriteToUse = Sprite.Create(myTexture, rec, new Vector2(0.5f, 0.5f), 100);

        gameIcon.sprite = spriteToUse;

        matchmaker.SetLocalPlayerProfile();
    }


   [SerializeField] GameObject mask;

    

    void  SendDataAndStartGameManual(string gameData)
   {
        JSONNode jsondata = JSON.Parse(gameData);

        UFO_TEMP_GAME_SETTINGS.Token = jsondata["token"];  
        UFO_TEMP_GAME_SETTINGS.Template_id = jsondata["templateID"];  
        UFO_TEMP_GAME_SETTINGS.UserName = jsondata["userName"];
        UFO_TEMP_GAME_SETTINGS.GameKey = jsondata["gameKey"];
        UFO_TEMP_GAME_SETTINGS.MaxPlayers = jsondata["maxPlayers"];

        String url = jsondata["profilePicLink"];

       // StartCoroutine(GetPlayerImageLocal(url,this.PlayerProfile)); 

         if(matchmaker != null) matchmaker.forceConnectPhoton();
   }







    class exampleJsonObject
    {
       public  GameData[] gamedata = new GameData[2] ;

       public exampleJsonObject(GameData _gameData)
       {
            
            this.gamedata [0] = _gameData;
       }
    }

    class GameData 
    {
        public string token;
        public string templateID;

        public string userName;
        public string userID;
        public string profilePictureLink;

        public string gameKey;

        public int maxPlayers;

      


    }







}

}
