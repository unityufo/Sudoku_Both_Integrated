﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameConfigList", menuName = "UFO/Game/GameConfigLst", order = 0)]

public  class GameConfigList : ScriptableObject
{
  [SerializeField] List<GameConfigData> gameConfigDataLIst;
  public GameConfigData GetGameConfigData(string gameKey)
  {
    foreach (var item in gameConfigDataLIst)
    {
        if(item.GameKey == gameKey)
        {
          return item;
        }
    }

     return null;  //maybe return a default game instead of null?
  }
   
}