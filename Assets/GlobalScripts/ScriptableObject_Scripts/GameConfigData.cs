using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DevLocker.Utils;
using UnityEngine.SceneManagement;



[CreateAssetMenu(fileName = "NewGameConfigData", menuName = "UFO/Game/GameConfigData", order = 0)]
public class GameConfigData : ScriptableObject 
{


  [SerializeField] SceneReference gameScene;
  [SerializeField] string gameKey;

  [SerializeField] Sprite gameBackGround;

  [SerializeField] GameRoomProperties gameCustomRoomProperties = null;  //if any custom room properties present that require to be loaded before creating room

///extra features  //incase if multiple scenes for a single game present but has any specific parameters to choose the scene// will be implemented later... 
 List<SceneReference> sceneList;

/////
  public string GameKey { get => gameKey;}
  public Sprite GameBackGroundImage { get => gameBackGround;}




  public int GetSceneIndex()
  {
    int  index =  SceneUtility.GetBuildIndexByScenePath(gameScene.ScenePath);   
    if(index == -1) Debug.LogError("Scene not added to buldndex");
    return index ; 
  }

  public string GetSceneName()
  {
    return gameScene.SceneName;
  }


 private void OnValidate() {  //the error is just as reminder to check incase the referenced scene is wrong scene//project convention : scenename == gamekey //not mandotary
  if(gameScene.SceneName != gameKey)
  {
    Debug.LogError("the game key and scenename is diffrent , was it intentional? ");
  }
 }

    
}

