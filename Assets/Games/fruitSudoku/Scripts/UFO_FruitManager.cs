﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace UFO.Games.fruitSudouku
{
    public class UFO_FruitManager : MonoBehaviour
{

    private void Start()
    {
        StartCoroutine(PostConfirm());
    }


    IEnumerator PostConfirm()
    {
   

        string url = "https://ufogames.webc.in/api/ufo/confirm-entry";
        WWWForm form = new WWWForm();
        form.AddField("photon_game_id",UFO_TEMP_GAME_SETTINGS.Photon_game_id);
            
           
        
         using (UnityWebRequest www = UnityWebRequest.Post(url,form))
        {
           // www.SetRequestHeader("Accept","application/json");
            www.SetRequestHeader("Authorization","Bearer " +  UFO_TEMP_GAME_SETTINGS.Token );

            var i = www.SendWebRequest();
      
            while(!i.isDone)
            {       

                Debug.Log("Waiting :  addTime maybe?" );
               
                 yield return null;
            }
            if (www.isNetworkError || www.isHttpError)
            {
                    Debug.Log(www.downloadHandler.text);
    
            }

            else Debug.Log("POstConfirm api called");

    
                
        }
    }
        int totalScore = 0;
        void AddScore(int i)
        {
            totalScore += i;
        }
        void SubmitScore(bool val)
        {
            int score = totalScore;
            StartCoroutine(SubmitScoreToServer(score));
        }

        [SerializeField] string Public_Key;

        
        IEnumerator SubmitScoreToServer(int score)
        {

      
    
            // byte[] array = System.Text.Encoding.UTF8.GetBytes(encryptedRSAData);
            // string base64text = System.Convert.ToBase64String(array);
   
    
        
            string url = "https://ufogames.webc.in/api/ufo/submit-result";
            WWWForm form = new WWWForm();
            form.AddField("photon_game_id",UFO_TEMP_GAME_SETTINGS.Photon_game_id);
            form.AddField("result","");
            form.AddField("score",score.ToString());
           
           
        
         using (UnityWebRequest www = UnityWebRequest.Post(url,form))
        {
           // www.SetRequestHeader("Accept","application/json");
            www.SetRequestHeader("Authorization","Bearer " + UFO_TEMP_GAME_SETTINGS.Token) ;

            var i = www.SendWebRequest();
      
            while(!i.isDone)
            {       

                Debug.Log("Waiting :  addTime maybe?" );
               
                 yield return null;
            }
            if (www.isNetworkError || www.isHttpError)
            {
                    Debug.Log(www.downloadHandler.text);
    
            }

            else Debug.Log("POstConfirm api called");
  
        }
    }

        
        private void OnEnable() {
            GameEvents.onGameOver += SubmitScore;
            GameEvents.onGetBonus += AddScore;
             GameEvents.onGetpoints += AddScore;
        }

        private void OnDisable() {
             GameEvents.onGameOver -= SubmitScore;
            GameEvents.onGetpoints -= AddScore;
            GameEvents.onGetBonus -= AddScore;

        }



}
    
}
