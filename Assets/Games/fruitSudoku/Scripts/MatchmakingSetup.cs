using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using  Hashtable = ExitGames.Client.Photon.Hashtable;

namespace UFO.Games.fruitSudouku
{
public class MatchmakingSetup : MonoBehaviourPunCallbacks
{
   
   [SerializeField] private string Version_Name = "0.1";
   [SerializeField] private GameObject UsernamMenu;
  

   [SerializeField] private InputField UsernameInput;
  
   [SerializeField] private GameObject UserN_Ok_Button;


    [Header("Matchmaking panel")]
   [SerializeField] GameObject matchmaking_Panel;
   [SerializeField] Text TitleMessageMM;
   [SerializeField] List<Text> playerListText = new List<Text>();

   [SerializeField] Text NetworkDebugText;

   [SerializeField] Button StartGameButton;




   [SerializeField] GameObject HybridModeButton;
   [SerializeField] GameObject Async_Mode_Button;


   //[SerializeField] GameObject waitingForHost;
   
   //[SerializeField] GameObject joiningRoom; // implement later

   
   
   
   
       void Awake()
    {
        StartGameButton.interactable = false;
        PhotonNetwork.ConnectUsingSettings();
        NetworkDebugText.text = "Cant Start game, Not connected to server";
    }

    private void Start() {
        Timer=matchMakingWaitTime;
        UsernameInput.characterLimit = 8;




       if(!PlayerPrefs.HasKey("playerName")) UsernamMenu.SetActive(true);
       else  {PhotonNetwork.NickName = PlayerPrefs.GetString("playerName"); Debug.Log(PlayerPrefs.GetString("playerName")); } ;   
       
       
       PhotonNetwork.AutomaticallySyncScene=true;
    }

    bool startMatchMaking = false;
    
    public float matchMakingWaitTime = 20;
    float Timer;
    bool hasEnoughplayerinRoom = false;
    bool isJoiningRoom;
    bool isCreatingRoom;
    
    bool sceneloadRequestsent=false; // just a bool ato avoid calling loadscene multiple times
    
    
    private void Update()  // remove these functions from update and use callbacks .....!
    {
         if(hasEnoughplayerinRoom  && PhotonNetwork.InRoom)
               {
                  if(PhotonNetwork.IsMasterClient  && !sceneloadRequestsent)
                  {
                    //PhotonNetwork.CurrentRoom.IsOpen = false; 
                    LoadSyncLevel();
                    sceneloadRequestsent=true;
                    return;
                  }
                
                }
       
        if(PhotonNetwork.IsMasterClient && PhotonNetwork.InRoom)
        {
            
           
           
            if(!startMatchMaking) return;

            if(startMatchMaking && !hasEnoughplayerinRoom)
            {
                if(Timer > 0.1f )
                {
                    Timer-= Time.deltaTime;
                    return;
                }
                if(Timer <=0.2)
                {
                    // if(PhotonNetwork.PlayerList.Length > 1 )
                    // {
                    //     // HybridModeButton.SetActive(true);
                        if(PhotonNetwork.IsMasterClient && !sceneloadRequestsent){sceneloadRequestsent=true; LoadSyncLevel();  Debug.Log("Synced level");}
                   
                    //      return;
                    //  } 
                    // else if(PhotonNetwork.PlayerList.Length <= 1)
                    // {
                    //      //.Async_Mode_Button.SetActive(true);
                    //      LoadSyncLevel();
                     
                    //      return;

                    //  } 
                ////ativate the start in async mode button
                }



                ///other stuffs?
         
            }

        }
       // else if(!PhotonNetwork.IsMasterClient )
        //{
            // if(!PhotonNetwork.InRoom && isJoiningRoom && !startMatchMaking)
            // {
            //     TitleMessageMM.text = " Joining Room ... "
            //    //if(joiningRoom.activeInHierarchy== false) joiningRoom.SetActive(true);
               
            // }
            // else if(PhotonNetwork.InRoom && startMatchMaking)
            // {
                
            //     TitleMessageMM.text = "Waiting for Host to Start Game";

            //   // if statement jusdt to avoid calling the method each 
            //  //update,later it will be changed to event driven artitecture
            // }

       // }

    }

    bool HasEnoughPlayerInRoom()
    {
        if(PhotonNetwork.PlayerList.Length == maxPlayers) return true;
        return false;

    }

    public  override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(Photon.Realtime.TypedLobby.Default);
        StartGameButton.interactable = true;
        NetworkDebugText.text = "Connected to Server";
        NetworkDebugText.color = Color.green;
        Debug.Log("Connected");
    }
    

    public void ChangeUserNameInput()
    {
        if(UsernameInput.text.Length >= 3)
        {
            UserN_Ok_Button.SetActive(true);
        }
        else
        {
            UserN_Ok_Button.SetActive(false);
        }
    }

    public void SetUserName()
    {
        UsernamMenu.SetActive(false);
        
        PhotonNetwork.NickName = UsernameInput.text;
        PlayerPrefs.SetString("playerName", UsernameInput.text);
        PlayerPrefs.Save();
    }




    // public void CreateGame()
    // {
        
    //     PhotonNetwork.CreateRoom(CreateGameInput.text,new RoomOptions(){ MaxPlayers = 5, EmptyRoomTtl = 300000 ,PlayerTtl =  300000  },null); //86400000 = 24 hrs
    // }
    // public void JoinGame()
    // {
    //     RoomOptions roomOptions =  new RoomOptions();
    //     roomOptions.MaxPlayers = 5;
    //     roomOptions.PlayerTtl=  300000 ;
       
    //    // PhotonNetwork.JoinOrCreateRoom(JoinGameInput.text,roomOptions,TypedLobby.Default);
    //    PhotonNetwork.JoinRoom(JoinGameInput.text);


    // }




     byte maxPlayers = 2;
     public void SetNoOfPlayer(int _no_of_Players)
     {
         maxPlayers = ((byte)_no_of_Players);
         Debug.Log(maxPlayers);
     }
    

    public void JoinRandomRoom()
    {
        GameSettings.Instance.SetIsREalTime(true);
        PhotonNetwork.JoinRandomRoom(null,maxPlayers);
        isJoiningRoom = true;
        MatchmakingFuctions();
        //PhotonNetwork.JoinRandomOrCreateRoom(null,maxPlayers,roomOptions :  new RoomOptions{MaxPlayers = maxPlayers},typedLobby: TypedLobby.Default);
    }

////////////////////////////////////////////////////////////////////////////////

    int randomlistValue;
    int randomSudokuDataValue;
    public override void OnJoinRandomFailed(short returnCode, string message)
    {       
            
            Debug.Log("No rooms found creating a room");
            isJoiningRoom = false;
            TitleMessageMM.text = "No Rooms Found ..Creating Room";

            //creating random indexes for choosing sudoku data//now doing from gameSetting///cahnge in later Iteration
            /////////////////////////////////////////////////////////////
           
        //    GameSettings.Instance.SetCorrectDataset(GameSettings.Instance.GetDIfficulty());
            Hashtable roomproperties = new Hashtable();
            roomproperties.Add("LI",GameSettings.Instance.GetListIndexValue());
            roomproperties.Add("SI",GameSettings.Instance.GetSudokuDataIndexValue());
           
           ////////////////////////////////////////////////////////////


            RoomOptions _roomOptions =  new RoomOptions();
            _roomOptions.MaxPlayers = maxPlayers;
            _roomOptions.CustomRoomProperties = roomproperties;
           
            PhotonNetwork.CreateRoom(null,roomOptions: _roomOptions,typedLobby: TypedLobby.Default);
            isCreatingRoom = true;
    }
   

    // public override void OnRoomListUpdate(List<RoomInfo> roomList)
    

////////////Temp functions///needs refactoring/////////////////////
    void MatchmakingFuctions()
    {

        if(!PhotonNetwork.InRoom && isJoiningRoom && !startMatchMaking)
            {
                matchmaking_Panel.SetActive(true);
                TitleMessageMM.text = " Joining Room ... ";
             
               
            }
            else if(!PhotonNetwork.InRoom && isCreatingRoom && !startMatchMaking)
            {
                    TitleMessageMM.text = "Creating  Room ... ";
            }


            else if(PhotonNetwork.InRoom && startMatchMaking && !PhotonNetwork.IsMasterClient)
            {
                if(!matchmaking_Panel.activeInHierarchy)matchmaking_Panel.SetActive(true);
                   TitleMessageMM.text = "Waiting for Host to Start Game";

             
            }
             else if(PhotonNetwork.InRoom && startMatchMaking && PhotonNetwork.IsMasterClient)
            {
                if(!matchmaking_Panel.activeInHierarchy)matchmaking_Panel.SetActive(true);
                   TitleMessageMM.text = "Waiting for Other Players";

             
            }
    }


    void ClearPlayerListField()
    {
        foreach (var playerText in playerListText)
        {
            playerText.text = "";
            
        }
    }

    void setPlayerList()
    {
        int i =0;
        var otherplayer = PhotonNetwork.PlayerListOthers;
        foreach (var playerText in playerListText)
        {
            if(i < otherplayer.Length) //i/0 logic!!!
            {
                playerText.text = otherplayer[i].NickName;
                i++;          
            }
            if(i > otherplayer.Length) return;
            
            
        }
    }




///////////////////////////////////////////////////////

public override void OnRoomListUpdate(List<RoomInfo> roomList)
{
    Debug.Log("A room was found ?");
    foreach (var item in roomList)
    {
        Debug.Log(item.Name);
        
    }
}


    
    [SerializeField] Text  roomIDTExt;

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
       // joiningRoom.SetActive(false);
       // startMatchMaking = true;

        ClearPlayerListField();
        setPlayerList();

        Debug.Log("new player Joined");

        hasEnoughplayerinRoom = HasEnoughPlayerInRoom();
            //  if(hasEnoughplayerinRoom  && PhotonNetwork.InRoom)
            //    {
            //       if(PhotonNetwork.IsMasterClient  && !sceneloadRequestsent)
            //       {
            //         //PhotonNetwork.CurrentRoom.IsOpen = false; 
            //         LoadSyncLevel();
            //         sceneloadRequestsent=true;
            //         return;
            //       }
                
            //     }
        
    }
   public override void OnPlayerLeftRoom(	Player 	otherPlayer	)	
     {
         hasEnoughplayerinRoom = HasEnoughPlayerInRoom();
           MatchmakingFuctions();
          ClearPlayerListField();
          setPlayerList();
  
     }

   
   
   
   
    public   override  void  OnJoinedRoom()
    {
        if(!PhotonNetwork.IsMasterClient && isJoiningRoom && !isCreatingRoom) //is master client checking cause an issue 
        {
            var randomlistindex = PhotonNetwork.CurrentRoom.CustomProperties["LI"];
            var randomSudokuDataindex = PhotonNetwork.CurrentRoom.CustomProperties["SI"] ;
            int i = (int)randomlistindex;
            int x = (int) randomSudokuDataindex;

            GameSettings.Instance.SetMultiplayerSudokuData(i,x);
        }
       
         TitleMessageMM.text = " Room Joined ";
         startMatchMaking = true;
         MatchmakingFuctions();
          ClearPlayerListField();
          setPlayerList();
          hasEnoughplayerinRoom = HasEnoughPlayerInRoom();

        roomIDTExt.text = "Room ID : " +  PhotonNetwork.CurrentRoom.Name;
       
        Debug.Log(PhotonNetwork.CurrentRoom.Name);
        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        // foreach (var item in PhotonNetwork.PlayerListOthers)
        // {
        //      Debug.Log(item.NickName);
        // }
        
//         messageTet.text = "Room joined" +  PhotonNetwork.CurrentRoom.Name;
        //PhotonNetwork.LoadLevel("game");
    }


    public static void LoadSyncLevel()
    {
        PhotonNetwork.CurrentRoom.IsOpen=false;
        PhotonNetwork.LoadLevel("Game");
        
        
    }

}

}