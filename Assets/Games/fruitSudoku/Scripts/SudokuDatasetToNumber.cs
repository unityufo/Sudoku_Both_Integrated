﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace UFO.Games.fruitSudouku
{
public class SudokuDatasetToNumber : MonoBehaviour
{
    // Start is called before the first frame update

    public string sudoMatrixNumber;

    public  Transform buttonListGrandParent;

   
   
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        // foreach (var item in sudoMatrixNumber)
        // {
        //     print(item);
            
        // }
        SetValuesToButtonsVersion2();
    }

    //A BASIC IMPLEMENTATION
    void SetValuesToButtons()
    {
        int index = 0;
        foreach (var rowsofButtons in buttonListGrandParent.GetComponentsInChildren<Transform>())
        {
            foreach (var button in rowsofButtons.GetComponentsInChildren<Transform>())
            {
                button.GetComponentInChildren<Text>().text = sudoMatrixNumber[index].ToString();
                index++;
                
            }
            
        }
    }


      void SetValuesToButtonsVersion2()
    {
        int index = 0;
        foreach (var rowsofButtons in buttonListGrandParent.GetComponentsInChildren<Transform>())
        {
            foreach (var button in rowsofButtons.GetComponentsInChildren<Transform>())
            {
                 foreach (var buttonchild in button.GetComponentsInChildren<Transform>())
            {
                buttonchild.GetComponentInChildren<Text>().text = sudoMatrixNumber[index].ToString();
                index++;
                
            }
                
            }
            
        }
    }



  
    
}


}