using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using  Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using UnityEngine.Networking;

using UFO.GlobalComponents;


namespace UFO.Games.fruitSudouku
{
public class ScoreHolder : MonoBehaviourPunCallbacks  // this class will handle all multiplayer score calculations and multiplayer  game logic related to score
{                                                     // 
    // Start is called before the first frame update
 [SerializeField] Text hi_scoreText;

 //[SerializeField] Text final_scoreText;
 //[SerializeField] Text GameOverScore_text;


//public static ScoreHolder instance;

public  int  hi_score = 0;

bool is_gameOver = false;

public static Hashtable playerproperties = new Hashtable();///change this to better ///avoid static


  Dictionary<string,PlayerStats> playerStats = new Dictionary<string, PlayerStats>();


private void Awake() {
    playerproperties.Clear();


    playerproperties.Add("GameOver",false);
    playerproperties.Add("GameFinished",is_gameFinsihed);
    
    playerproperties.Add("PlayerScore", hi_score);

    PhotonNetwork.LocalPlayer.SetCustomProperties(playerproperties);
    
    
// if(instance == null)
//     {
//        DontDestroyOnLoad(this);
//        instance = this;
//     }
//     else Destroy(this);



}
class testclass
{
    float time;
}
private void Start() {
    StartCoroutine(testroutine(timearray));
}


int index = 0;
float  i=0;
public float[] timearray = new float[5];
IEnumerator testroutine(float[] timearray)
{
    float temtime = timearray[index];
   
    while(i < temtime)
    {
        i += Time.deltaTime;
        yield return null;
    }
    Debug.Log("TEST ROUTINE INDEX VALUE IS : " + index);
   if(index < timearray.Length-1)
   {
       i = 0;
       index++;
       StartCoroutine(testroutine(timearray));
   }
    

}


public  void AddScore(int points)
{
   
    hi_score += points;
    UpdateText(hi_score);
    
    
    playerproperties["PlayerScore"] = hi_score;
     PhotonNetwork.LocalPlayer.SetCustomProperties(playerproperties);
    
    
}
public  void AddBonusScore(int points)
{
    Debug.Log("Added Bonus  points");
    hi_score += points;
    UpdateText(hi_score);
    
    playerproperties["PlayerScore"] = hi_score;
    PhotonNetwork.LocalPlayer.SetCustomProperties(playerproperties);
    
}


public  void SetScore(int score)
{
            hi_score = score;
           
}

public void  SetGameOVerBool(bool gameover)
{
       is_gameOver = gameover;
}

public void UpdateText(int score)
{
  //  int temp = hi_score;

   hi_scoreText.text =  hi_score.ToString("00");       // Mathf.Lerp(float(hi_score)) score.ToString();
}
 private void Update() 
 
 {
    if(!is_gameOver) return;

    //var score = (float) hi_score;
    // int lerpValue ;
    //  if(lerpValue <= )
//    Debug.Log(lerpValue);
   // final_scoreText.text = hi_score.ToString("00");

    
     
 }

 public Text game_win_ScoreText;

void SetGameWinScore(bool inputbool)
{
   //  game_win_ScoreText.text = hi_score.ToString("00"); //////////cahnge this methoids ///clean up!!
}
void SetGameOverScore(bool input)
{
   // GameOverScore_text.text = hi_score.ToString("00");
}
void  SendGameOverCall(bool val)
{
   
    Invoke("SendOnGameOverCallback", 4f);
    

}

void SendOnGameOverCallback()
{
    if(GlobalGameManager._instance != null) GlobalGameManager._instance.SendGameCompletedCall();
}

 public override void OnEnable() {
     base.OnEnable();
     GameEvents.onGetpoints += AddScore;
     GameEvents.onGetBonus += AddBonusScore;
     GameEvents.onGameOver += SetGameOVerBool;
   
   
     GameEvents.onGameWin += SetGameWinScore;
     GameEvents.onGameLost += SetGameOverScore;

      GameEvents.onGameOver += SendGameOverCall;
 }
 public  override void OnDisable()
  {
      base.OnDisable();
     GameEvents.onGetpoints -= AddScore;
     GameEvents.onGetBonus -= AddBonusScore;
     GameEvents.onGameOver -= SetGameOVerBool;
     

     GameEvents.onGameWin -= SetGameWinScore;
     GameEvents.onGameLost += SetGameOverScore;

      GameEvents.onGameOver -= SendGameOverCall;
  }



  ///////////////////////MultiplayerFunctions///

  public List<Text> player_scoresTextlist = new List<Text>(); ///later create a new uiscoreclass to easily customise
    bool is_gameFinsihed=false; //whether the game is completed by finishing all boxes

    bool resultUpdated= false;


    [SerializeField] List<Text> gameWinTextList = new List<Text>();
    [SerializeField] Text LocalScoreText_Fail;
     [SerializeField] Text LocalScoreText_Win;
    [SerializeField] List<Text> gameOverTextList = new List<Text>();

    [SerializeField] GameObject DrawGame;

    
    
    ////on next iteration create a serialisation methods to only update the changedprops instead of rechecking the whole .....!! priority : low
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {




///////////////////temp solution////////////////////////////////////////////////////////
        
        
        foreach (var item in PhotonNetwork.PlayerListOthers)
        {
            
          
                    PlayerStats player_stat_info = new PlayerStats();
                    player_stat_info.name = item.NickName;
                    player_stat_info.score = (int)item.CustomProperties["PlayerScore"];
                    player_stat_info.isGameOver = (bool)item.CustomProperties["GameOver"];

                   if(playerStats.ContainsKey(item.NickName))
                   {
                        playerStats[item.NickName] = player_stat_info;
                   }  
                   else playerStats.Add(item.NickName,player_stat_info);
                    
               

        }

        /////////////////////////////////////////////////////////////////////////////////////////
        if(is_gameOver)
        {
            ClearPlayerListField(gameOverTextList);
            SetCusstomPlayerListText(gameOverTextList);
            ClearPlayerListField(gameWinTextList);
            SetCusstomPlayerListText(gameWinTextList);
        }
        
        ClearPlayerListField();


        //::::::::::::::::::::::::  Caution  ::::   Remove IT ::::::::::::::::::::::::::::::::::::::    
        if(!GameSettings.Instance.GetIsRealTime()) return; // just for testingPurpose///remove it!
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        SetPlayerList();


        if(resultUpdated) return;

        bool othersFinished = CheckOthersFinishedGame();

        if(!is_gameOver && othersFinished)
        {
            CheckScore_IfHigher();
           
        }
        
        if(is_gameOver && !othersFinished)
        {
            CheckScore_IfLow();
           
        }

        if(is_gameOver && othersFinished)  // just tfor exception handling ..i guess...cross check if it makes sense later !!!
        {
                CompareScore();
        }
    }

     public override void OnPlayerLeftRoom(	Player 	otherPlayer	)	
     {
         var _customProperty = otherPlayer.CustomProperties;
         _customProperty["GameOver"] = true;
         
         otherPlayer.SetCustomProperties(_customProperty);

            ////////////////////
         var stat =    playerStats[otherPlayer.NickName];
         stat.isGameOver=true;
         playerStats[otherPlayer.NickName] = stat;
     }
    
    void ClearPlayerListField()
    {
        int i = 0;
        int maxplayers = GameSettings.Instance.maxPlayerPossible;
        foreach (var playerText in player_scoresTextlist)
        {
           if(i <= maxplayers ){ playerText.text = "";}

           else if(i > maxplayers) playerText.text = "";
           
            
        }
    }

    void SetPlayerList()
    {
        // int i =0;
        // var otherplayer = PhotonNetwork.PlayerListOthers;
        // foreach (var playerText in player_scoresTextlist)
        // {
        //     if(i < otherplayer.Length) //i/0 logic!!!
        //     {
        //         playerText.text = otherplayer[i].NickName + "  :  " + otherplayer[i].CustomProperties["PlayerScore"].ToString();
        //         i++;          
        //     }
        //     if(i > otherplayer.Length) return;
            
            
        // }


        int i = 0;
        if(playerStats == null) return;
      foreach (var item in playerStats)
    {
        player_scoresTextlist[i].text = item.Value.name + " : " +  item.Value.score.ToString();
        
        Debug.Log(item.Value.name);
        i++;
    }
    
    
    }
    
public  bool CheckOthersFinishedGame()
{
    foreach (var item in playerStats)
    {
        bool game_overbool = (bool) item.Value.isGameOver;
        
        if(game_overbool == false)    return false;
        
    }

    return true;
}

   void CompareScore() //compares the scores to make final decision;
   {
       List<int> templist = new List<int>();
       
       foreach (var item in playerStats)
       {
           int tempvar = (int)item.Value.score;
           templist.Add(tempvar);
       }
      
       templist.Sort();

       
       int otherPlayer_highest_score = templist[templist.Count -1];
       if(this.hi_score > otherPlayer_highest_score)
       {
           
            GameEvents.onGameWinMethod(true);
            
            resultUpdated=true;

       }
        else if(this.hi_score == otherPlayer_highest_score)
       {
         
         DrawGame.SetActive(true);
         //  GameOverScore_text.text = hi_score.ToString("00");
           resultUpdated=true;

       }
       else if(this.hi_score < otherPlayer_highest_score)
       {
           GameEvents.onGameLostMethod(true);
       }


   }


   void CheckScore_IfLow() //checks whether this players  score is lower than the lowest scored other player  in room
   {
         List<int> templist = new List<int>();
       
       foreach (var item in playerStats)
       {
           int tempvar = (int)item.Value.score;
           templist.Add(tempvar);
       }
      
        templist.Sort();
        templist.Reverse();
        int otherPlayer_highest_score = templist[templist.Count -1];
        if(hi_score < otherPlayer_highest_score )
        {
            GameEvents.onGameLostMethod(true);
             resultUpdated=true;
        }
   }
    void CheckScore_IfHigher() // chec this players score is higher than the the other players who's gameOver;
   {
         List<int> templist = new List<int>();
       
       foreach (var item in playerStats)
       {
           int tempvar = (int)item.Value.score;
           templist.Add(tempvar);
       }
      
        templist.Sort();
        
        int otherPlayer_highest_score = templist[templist.Count -1];
        if(hi_score > otherPlayer_highest_score )
        {
            GameEvents.onGameWinMethod(true);
             resultUpdated=true;
        }
   }


   string winner=null;//////////////implement later

   public string GetWinner()
   {
       return winner;
   }

void SetGame_isFinished(bool isfinished)
{
    is_gameFinsihed = isfinished;
    playerproperties["GameFinished"] = is_gameFinsihed;
    PhotonNetwork.SetPlayerCustomProperties(playerproperties);




}


 void SetCusstomPlayerListText(List<Text> textlist)
    {
       

        int i = 0;
      foreach (var item in playerStats)
    {
        textlist[i].text = item.Value.name + " : " +  item.Value.score.ToString();
        
        Debug.Log(item.Value.score);
        i++;
    }
    }

void ClearPlayerListField(List<Text> listText)
    {
       
       // int maxplayers = GameSettings.Instance.maxPlayerPossible;
        foreach (var playerText in listText)
        {
           
           playerText.text="";
            
        }
    }
  


 }

 [System.Serializable]
 public class PlayerStats
 {
     public string name;
     public int score;

     public bool isGameOver = false;


 }



}