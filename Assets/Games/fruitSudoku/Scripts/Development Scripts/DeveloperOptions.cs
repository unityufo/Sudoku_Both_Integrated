﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


namespace UFO.Games.fruitSudouku
{
public class DeveloperOptions : MonoBehaviour
{
    // Start is called before the first frame update


    public Button unhide_solution_button; 

    public Dropdown dropDownMinute;
    public Dropdown dropDownSecond;

    public Button SetTimeButton;


    bool active = false;
    void Start()  // remove listeners later !!
    {
      if(unhide_solution_button !=null)  unhide_solution_button.onClick.AddListener(()=>  activate());
    if(dropDownMinute!=null)      dropDownMinute.onValueChanged.AddListener(delegate { onValueMinute(dropDownMinute);});
    if(dropDownSecond!=null)    dropDownSecond.onValueChanged.AddListener(delegate { onValueSecond(dropDownSecond);});
    if(SetTimeButton!=null) SetTimeButton.onClick.AddListener(delegate {SetGameTime();});
    
    }

    // Update is called once per frame
        float minute = 3;
        float seconds = 0;
        
        public  void onValueMinute(Dropdown input)
        {
           minute =  float.Parse(input.options[input.value].text);
           print("value changed   : " + minute.ToString()  +  "minutes" );

        }
         public void onValueSecond(Dropdown input)
        {
           seconds =  float.Parse(input.options[input.value].text) ;
           print("value changed   : " + seconds.ToString()  + "seconds" );

        }
        public void  SetGameTime()
        {
            GameSettings.Instance.SetGameTime(minute,seconds);

        }


     public   void activate()
       {
           active=!active;
          

           Unhide_Solution(active);
       } 




    void Unhide_Solution(bool unhide)
    {
        if(unhide)
        {
            foreach (var item in SudokuGrid.getSudokuGrid())
                {
                    var square = item.GetComponent<GridSquare>();
            
                    if (!square.GetHasDefaultValue()) //{  //continue;
                    {square.isDeveloperOptionOn=true; square.ChangeTextColour(Color.green); square.ShowValue();}
                }

        }
        if(!unhide)
        {
            foreach (var item in SudokuGrid.getSudokuGrid())
                {
                    var square = item.GetComponent<GridSquare>();
            
                    if (!square.GetHasDefaultValue()) //continue;
                    { square.ChangeTextColour(Color.black); square.HideText(); square.isDeveloperOptionOn=false;}
                }

        }
        
    }


    public void MainMenu()
    {
       // GameSettings.Instance.DestroyThis();
        var instances = FindObjectsOfType<GameSettings>();
        //var dontdestorinstances = FindObjectsOfType<DontDestroyOnLoad>();
        foreach (var item in instances)
        {
            Debug.Log(item);
            Destroy(item.gameObject);
            if(item!=null) Destroy(item);
        }
        Debug.Log("DestroyImmediate");
        SceneManager.LoadScene(0);
    }
}
}