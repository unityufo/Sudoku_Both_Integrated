﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UFO.Games.fruitSudouku
{

    public class SudokuGrid : MonoBehaviour
{
    public int columns = 0;
    public int rows=0;
    public float square_offset=0.0f;
    public GameObject grid_square;
    public Vector2 start_position = new Vector2(0.0f,0.0f);
    public float square_scale = 1.0f;
     [SerializeField] float square_gap = 0.1f;

    static Color line_highlightColor = Color.red;
     [SerializeField] Color linehighlightPick;
  static Color highLightColour = Color.red;
  [SerializeField] Color highLightColorPIck;
  
        private   List<GameObject> grid_Squares = new List<GameObject>();

        static List<GameObject> grid_Squaresstatic  = new List<GameObject>();

    public static  List<GameObject> getSudokuGrid()
    {
        
        return grid_Squaresstatic;
    }


    public string sudokuMatrix; // later change to scriptable data set 
    public string SolvedsudokuMatrix;

     [SerializeField] GameObject backboard;
     
     static SudokuData _sudokuData;
        private SudokuData Default_sudokuData;

        public GameObject NoteMessageText;

        bool note_active;
public void OnNoteActive(bool active )
{
    note_active = active;
    NoteMessageText.SetActive(note_active);
}




        // Start is called before the first frame update





void OnValidate() {
    highLightColour = highLightColorPIck;
    line_highlightColor= linehighlightPick;
}

        void Start()
    {
        highLightColour = highLightColorPIck;
        line_highlightColor= linehighlightPick;
        
        grid_Squaresstatic = grid_Squares;

        _sudokuData = GameSettings.GetDataset(); //TempStaticSolution.GetSudokuData();//GameSettings.GetDataset();
        Debug.Log(_sudokuData.solvedData);
       if(!backboard.activeInHierarchy) backboard.SetActive(true);
        if(grid_square.GetComponent<GridSquare>()==null) Debug.LogError("The  gameobject has to have grid script");

            CreateGrid();
    //   if(_sudokuData !=null)  SetGridNumber(_sudokuData);
    //   else {  Debug.LogError("using Default data");SetGridNumber(Default_sudokuData);}
        

    }   

    bool canstartGame = false;

    public void SetupStuff()
    {
        _sudokuData = GameSettings.GetDataset();
        SetGridNumber(_sudokuData);
        canstartGame = true;
    }

     void Update()
    {
       if(!canstartGame) return;


        if(Input.GetMouseButtonDown(0))
        {
           GameObject obj = EventSystem.current.currentSelectedGameObject;
            if(obj ==null)
            {  
                SetSquareColor(LineHighlighter.instance.GetAllSquareIndexes(),Color.black);  
                foreach (var item in grid_Squaresstatic)
                {
                    GridSquare  gridSquare = item.GetComponent<GridSquare>();

                   gridSquare.SetShake(false);    
          
          
                }
                
            }
            
            
           
        }
    }

   


    private void CreateGrid()
    {
       SpawnGridSquares();
        SetSquarePosition();
    }


    // Update is called once per frame
    private void SpawnGridSquares()
    {
        int square_indexx = 0; 
       
        for(int row =0; row < rows; row ++)
        {
            for(int column =0; column < columns; column ++)
            {
                 grid_Squares.Add(Instantiate(grid_square) as GameObject);

                 grid_Squares[grid_Squares.Count -1].GetComponent<GridSquare>().SetSquareIndex(square_indexx);

                 grid_Squares[grid_Squares.Count -1].transform.SetParent(this.transform); //parent= this.transform; //may have a bug( count -1) for final iteration //check!! //will solve later

                 grid_Squares[grid_Squares.Count - 1].transform.localScale = new Vector3(square_scale,square_scale,square_scale);

                square_indexx++;
            }
        }

    }
   
     private void SetSquarePosition() //suggestion:  improve the algorithm or make sure it handles all screen size diffrences and ratios 
    {
       var square_rect = grid_Squares[0].GetComponent<RectTransform>();
        Vector2 offset = new Vector2();
        Vector2 square_gapNumber = new Vector2(0.0f,0.0f);
        bool row_moved = false;
        offset.x = square_rect.rect.width * square_rect.transform.localScale.x + square_offset;
        offset.y = square_rect.rect.height * square_rect.transform.localScale.y + square_offset;

        int column_number = 0;
        int row_number = 0;

        foreach(GameObject square in  grid_Squares)
        {
            if(column_number  + 1 > columns)
            {
                row_number ++;
                column_number = 0;
                square_gapNumber.x=0;
                row_moved=false;
            }

            var pos_x_offset = offset.x * column_number + (square_gapNumber.x * square_gap);
            var pos_y_offset = offset.y * row_number + (square_gapNumber.y * square_gap);

            if(column_number >0 && column_number%3 ==0)
            {
                square_gapNumber.x ++;
                pos_x_offset += square_gap;
            }
            if(row_number > 0 && row_number % 3 ==0 && row_moved == false)
            {
                row_moved = true;
                square_gapNumber.y++;
                pos_y_offset += square_gap;
            }

            square.GetComponent<RectTransform>().anchoredPosition = new Vector2(start_position.x + pos_x_offset,start_position.y - pos_y_offset);
            column_number++;
        }



    }
    private void SetSquarePositionOLdMethod() //suggestion:  improve the algorithm or make sure it handles all screen size diffrences and ratios 
    {
       var square_rect = grid_Squares[0].GetComponent<RectTransform>();
        Vector2 offset = new Vector2();
        offset.x = square_rect.rect.width * square_rect.transform.localScale.x + square_offset;
        offset.y = square_rect.rect.height * square_rect.transform.localScale.y + square_offset;

        int column_number = 0;
        int row_number = 0;

        foreach(GameObject square in  grid_Squares)
        {
            if(column_number  + 1 > columns)
            {
                row_number ++;
                column_number = 0;
            }

            var pos_x_offset = offset.x * column_number;
            var pos_y_offset = offset.y * row_number;

            square.GetComponent<RectTransform>().anchoredPosition = new Vector2(start_position.x + pos_x_offset,start_position.y - pos_y_offset);
            column_number++;
        }


    }
   ////////////////////////check input to set the value it might be wher ethe iussue is;

   
    private void SetGridNumber(SudokuData data)  //add error handler later  // prone to error if < 81
    {
 float i =0;
        //suggestion: add a data checker to make sure the input data is passable,and if not use backup matrix 
        //throw the data mismach error();
        int tempI=0;
        foreach(var square in grid_Squares)
        {
            var sq = square.GetComponent<GridSquare>();
            sq.GetComponent<Button>().interactable = true;
                sq.StartSweepAnimation(i);
                i = i + 0.002f;
           // square.GetComponent<GridSquare>().SetNumber(UnityEngine.Random.Range(0,10));
           square.GetComponent<GridSquare>().SetNumber(int.Parse(data.unsolvedData[tempI].ToString()) );
           square.GetComponent<GridSquare>().SetCorrectNumber(int.Parse(data.solvedData[tempI].ToString())); // change this later!!
           grid_Squares[tempI].GetComponent<GridSquare>().SetHasDefaultValue(data.unsolvedData[tempI] != 0 && data.unsolvedData[tempI] == data.solvedData[tempI]);
           
           if(grid_Squares[tempI].GetComponent<GridSquare>().GetHasDefaultValue())
           {
               Color32 darkPurple = new Color32(0x2F, 0x5E, 0xA6, 0xFF);// 9976B0   //2F5EA6
               grid_Squares[tempI].GetComponent<GridSquare>().ChangeTextColour(darkPurple);
           }
           
           if(tempI < (rows * columns)) tempI++;
        }
        
    }

    //  
    static void   Highlight(int value) //maybe further optimse with a dictionary to avoifd getiing component at runtime//check later
    {
        SetSquareColor(LineHighlighter.instance.GetAllSquareIndexes(),Color.black);
        int selected_gridvalue = 0;

       // grid_Squares[1].GetComponent<GridSquare>().getgridvalue();
        foreach (var item in grid_Squaresstatic)
        {
            GridSquare  gridSquare = item.GetComponent<GridSquare>();

            gridSquare.SetShake(false);    
            if(gridSquare.getSquareIndex()== value) selected_gridvalue = gridSquare.getgridvalue();
          
        }
        print(selected_gridvalue);
        
        foreach (var square in grid_Squaresstatic)
        {
            
          var  grid_square = square.GetComponent<GridSquare>();
            //print(grid_square_value);
           if(grid_square.getgridvalue() == selected_gridvalue && grid_square.GetHasDefaultValue() && grid_square.getgridvalue() !=0 && !grid_square.GetHasWrongNumber())
           {
               print("HIGJLIGHT FUNC");
               var colors = grid_square.gridbutton.colors;
             // #  CDFFCC  E2FFE1
            //   Color32 lightGreen = new Color32(0xE2, 0xFF, 0xE1, 0xFF);
            //     colors.normalColor = Color.green; //lightGreen ;                             ///////////////kaboomchange
            //     grid_square.gridbutton.colors =  colors;

               grid_square.SetShake(true);

           }

           else{ if(!grid_square.GetHasWrongNumber()){
               var colors = grid_square.gridbutton.colors;
                colors.normalColor = Color.black;
                grid_square.gridbutton.colors = colors; 
                grid_square.SetShake(false);                      ///////////kaboomchange
                }}
        }

        OnSquareSelectedd(value);
    }


    private void OnEnable() {
        GameEvents.OnsquareSelected += Highlight;
        GameEvents.OnRightAnswer += CheckForBoxesSolved;
        GameEvents.OnNotesActive += OnNoteActive;
       // GameEvents.OnsquareSelected += OnSquareSelectedd;

       GameEvents.onGameStart +=    ()=>{canstartGame = true; SetupStuff(); };
        
    }


        private void OnDisable() {
        GameEvents.OnsquareSelected -= Highlight;
        GameEvents.onUpdateSquareNumber -= CheckForBoxesSolved;
         GameEvents.OnNotesActive -= OnNoteActive;
       // GameEvents.OnsquareSelected += OnSquareSelectedd;
        
    }

     static  void SetSquareColor(int[] data,Color col)
    {
        // foreach (var item in grid_Squaresstatic)
        // {
        //     item.GetComponent<GridSquare>().isLineHighlighted=false;
        // }
        foreach (var index in data)
        {
            var comp = grid_Squaresstatic[index].GetComponent<GridSquare>();
            if( comp.getSquareIndex() != GameEvents.currentGrid && !comp.GetHasWrongNumber())
            {
                comp.SetSquareColor(col);
                //comp.isLineHighlighted = true;
            }
        }
    }

        private static void OnSquareSelectedd(int square_Index)
        {
           var GetHorizontalLine =LineHighlighter.instance.GetHorizontalLine(square_Index);
           var GetVerticalLine = LineHighlighter.instance.GetVerticalLine(square_Index);
            var square  = LineHighlighter.instance.GetSquare(square_Index);
           
           var allsquares = SudokuGrid.getSudokuGrid();    //////////change these later
            //F0F7FF
            //292A2C
          Color32 lightBlue = new Color32(0x29, 0x2A, 0x2C, 0xFF);

           // SetSquareColor(LineHighlighter.instance.GetAllSquareIndexes(),line_highlightColor);
           SetSquareColor(GetHorizontalLine,lightBlue);
           SetSquareColor(GetVerticalLine,lightBlue);
           SetSquareColor(square,lightBlue);

        //    foreach (var item in grid_Squaresstatic)
        //    {
        //      var squareGridd =   item.GetComponent<GridSquare>();
        //     //  if(!squareGridd.isLineHighlighted && !squareGridd.IsSelected() && !squareGridd.GetHasWrongNumber())
        //     //  {
        //     //      var colors = squareGridd.colors;
        //     //     colors.normalColor = Color.white;
        //     //     squareGridd.colors = colors;
        //     //  }
        //    }
        }



///this part checks whether a vertical line or meta box is completed to recieve extra points

        private void CheckForBoxesSolved(int square_Index)
        {
            var GetHorizontalLine =LineHighlighter.instance.GetHorizontalLine(square_Index);
            bool HorizontalLineSolved = CheckBoxesSolved(GetHorizontalLine);

            var GetVerticalLine = LineHighlighter.instance.GetVerticalLine(square_Index);
            bool verticalLineSolved = CheckBoxesSolved(GetVerticalLine);

            var square  = LineHighlighter.instance.GetSquare(square_Index);
            bool squareSolved = CheckBoxesSolved(square);

            if(HorizontalLineSolved) { Debug.Log("horizontal solved"); RunFadeAnimation(GetHorizontalLine,"horizontal"); GameEvents.OnGetBonusPOintMethod(50);}
            if(verticalLineSolved){Debug.Log("Vertical solved");RunFadeAnimation(GetVerticalLine,"vertical"); GameEvents.OnGetBonusPOintMethod(50);}
            if(squareSolved){Debug.Log("Square solved"); RunFadeAnimationSquare(square); GameEvents.OnGetBonusPOintMethod(50);};


            CheckALLBoxes();



        }

        bool CheckBoxesSolved(int[] data)
        {
            foreach (var item in data)
            {
                var square = grid_Squaresstatic[item].GetComponent<GridSquare>();
                if(!square.GetHasDefaultValue()) return false;

            }
            return true;

        }


        void CheckALLBoxes()
        {
            foreach (var item in grid_Squaresstatic)
            {
                if(!item.GetComponent<GridSquare>().GetHasDefaultValue()) return ;
            }

           GameEvents.OnGameOverMethod(true) ;
           TimeAndLives.setGameOver();
           GameEvents.OnGameFinishedMethod(true);
        }
           [SerializeField] float effect_interval;
    
    void RunFadeAnimation(int[] data,string direction)
        {
            if(direction == "vertical")Array.Reverse(data);
            float time =0;
            foreach (var item in data)
            {
                var square = grid_Squaresstatic[item].GetComponent<GridSquare>();
                square.StartSweepAnimation(time);
               // if(!square.GetHasDefaultValue()) return false;
               time +=  effect_interval;

            }
            //return true;

        }

        void RunFadeAnimationSquare(int[] data)
        {
            foreach (var item in data)
            {
                Debug.Log(item);
            }
           int[] order = new int[] {0,1,2,5,8,7,6,3,4};
           int[] temparay = new int[9];
            int index = 0;
            float time =0;
            
         
            foreach (var item in order)
            {
                 temparay[index] = data[item];
                //data[item]= temparay[index];
                // data[index] = temparay[order[index]];
                // print(data[index] + "  :  " + temparay[order[index]] + "  : " + index   + " : order : "+ order[index]);
                index ++;
            }

            foreach (var item in temparay)
            {
                var square = grid_Squaresstatic[item].GetComponent<GridSquare>();
                square.StartSweepAnimation(time);
               // if(!square.GetHasDefaultValue()) return false;
               time +=  effect_interval;

            }
            //return true;

        }

        [SerializeField] float finalsweepanimationInterval;
        IEnumerator WholeGridSweepAnimation(float time,string direction)
        {
             time =0;
            foreach (var item in getSudokuGrid())
            {
                var square = item.GetComponent<GridSquare>();
                square.StartSweepAnimation(time);
               // if(!square.GetHasDefaultValue()) return false;
               time +=  finalsweepanimationInterval;

            }

            return null;
        }
    


}

    
}
