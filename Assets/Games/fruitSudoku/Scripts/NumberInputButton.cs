﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UFO.Games.fruitSudouku
{
    public class NumberInputButton : Selectable ,IPointerClickHandler,ISubmitHandler,IPointerUpHandler,IPointerExitHandler
{
    // Button numberbutton;
    public int value = 0;
   

   
    public void OnPointerClick(PointerEventData eventData)
    {
       GameEvents.UpdateSquareNumberMethod(value);
     if(GameEvents.isNoteMode)         {

                foreach (var item in SudokuGrid.getSudokuGrid())
                    {
                        var square =item.GetComponent<GridSquare>();
                           if(square.getSquareIndex() == GameEvents.currentGrid && !square.GetHasDefaultValue() )
                             {
                                  item.GetComponent<GridSquare>().SetNoteSingleNumberValue(value);
                              }
                    }
                 }
    }

    public void OnSubmit(BaseEventData eventData)
    {
       
    }

    // Start is called before the first frame update

    // Update is called once per frame
   

    


}

}
