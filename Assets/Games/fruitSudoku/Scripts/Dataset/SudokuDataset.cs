﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UFO.Games.fruitSudouku
{

[CreateAssetMenu(fileName = "SudokuDataset", menuName = "Sudoku_Test/Create Dataset", order = 0)]

public class SudokuDataset : ScriptableObject
{
    [SerializeField]  Difficulty Difficulty_Level;
    [SerializeField]   List<SudokuData> sudokuDataLIst = new List<SudokuData>();

    public int getIndex{ get => sudokuDataLIst.Count;}


public Difficulty GetDifficulty()
{
    return Difficulty_Level;
}
    public SudokuData GetSudokuData(int  index)  // implement this later
    {
        return  sudokuDataLIst[index];
    }

    //  public SudokuData getSolvedData(int  index)  // implement this later
    // {
    //     return  sudokuDataLIst[index];
    // }

}

public enum Difficulty
{
    easy,medium,hard
}


[System.Serializable]

    
 public class SudokuData 

{
    public string unsolvedData;

    public string solvedData;


}
}