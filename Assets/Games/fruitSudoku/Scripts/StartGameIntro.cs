﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGameIntro : MonoBehaviour
{


    [SerializeField]GameObject panel;

   [SerializeField] Text counter;
    [SerializeField] Text message;
   
    Text errorMessage;

    [SerializeField] float Maxtimer;
    public  IEnumerator StartCountDown()
    {
       panel.SetActive(true); 
       counter.text = "3";
       
       yield return new WaitForSeconds(1);

       counter.text = "2";

       yield return new WaitForSeconds(1);
       counter.text = "1";

       yield return new WaitForSeconds(1);

       counter.text = "..."; 
        yield return new WaitForSeconds(1);
        panel.SetActive(false); 

    }



}
