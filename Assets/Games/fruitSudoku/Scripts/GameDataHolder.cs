﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UFO.Games.fruitSudouku
{
public class GameDataHolder : MonoBehaviour
{
     [SerializeField] List<SudokuDataset> _sudoku_datasetList = new List<SudokuDataset>();
     
     static List<SudokuDataset> staticDataset = new List<SudokuDataset>();

     public static List<SudokuDataset> GetSudokuDatasets()
     {
        return staticDataset; 
        // return _sudoku_datasetList;
     }
    // Start is called before the first frame update
    void Start()
    {
        staticDataset = _sudoku_datasetList;
    }

   
}

}
