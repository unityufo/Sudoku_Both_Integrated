﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UFO.Games.fruitSudouku
{
public class GameEvents : MonoBehaviour
{
   public static int  currentGrid;
   public static bool isNoteMode = false;



   private void Awake() {
    isNoteMode = false;
   }
    public delegate void CanStartGame();

    public static event CanStartGame onGameStart;


    public static void CanGameStartMethod()
    {
        if(onGameStart != null)
            onGameStart(); 
    }





    public delegate void UpdateSquareNumber(int number);

    public static event UpdateSquareNumber onUpdateSquareNumber;


    public static void UpdateSquareNumberMethod(int number)
    {
        if(onUpdateSquareNumber != null)
            onUpdateSquareNumber(number);
    }

    public delegate void SquareSelected(int square_Index);
    public static event SquareSelected OnsquareSelected;

 
    public static void SquareSelectedMethod(int square_Index)
    {
        if(OnsquareSelected != null) OnsquareSelected(square_Index);
            
    }

    public delegate void RightAnswer(int square_Index);
    public static event RightAnswer OnRightAnswer;

 
    public static void OnRightAnswerMethod(int square_Index)
    {
        if(OnRightAnswer != null) OnRightAnswer(square_Index);
            
    }

    public delegate void WrongNumber();
    public static event WrongNumber onWrongNumber;

    public static void OnWrongNumberMethod()
    {
        if(onWrongNumber != null) onWrongNumber();
    }

   //////

   public delegate void NotesActive(bool active);
   public static event NotesActive OnNotesActive;

   public static void OnNotesActiveMethod(bool active)
   {
       if(OnNotesActive!=null) OnNotesActive(active);
   } 

    public delegate void   gameover(bool gameover);
   public static event gameover onGameOver;

   public static void OnGameOverMethod(bool gameover)
   {
       if(onGameOver!=null) onGameOver(gameover);
   } 

   public delegate void   OnGetPoint(int points);
   public static event OnGetPoint onGetpoints;

   public static void OnGetPointMethod(int value)
   {
       if(onGetpoints!=null) onGetpoints(value);
   } 


    public delegate void   OnGetBonus(int points);
   public static event OnGetBonus onGetBonus;

   public static void OnGetBonusPOintMethod(int value)
   {
       if(onGetpoints!=null) onGetBonus(value);
   } 


    public delegate void   OnGameWin(bool value);
   public static event OnGameWin onGameWin;

   public static void onGameWinMethod(bool inputbool)
   {
       if(onGameWin!=null) onGameWin(inputbool);
   } 
            /////using multiple delegate signature of same type because for later alterations
public delegate void   OnGameLost(bool value);
   public static event OnGameLost onGameLost;

   public static void onGameLostMethod(bool inputbool)
   {
       if(onGameLost!=null) onGameLost(inputbool);
   } 

    public delegate void   OnGameFinished(bool value);
   public static event OnGameFinished onGameFinished;

   public static void OnGameFinishedMethod(bool inputbool)
   {
       if(onGameFinished!=null) onGameFinished(inputbool);
   } 

}

}