﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UFO.Games.sudoku
{
    public class ScoreTextDisplay : MonoBehaviour
{
    [SerializeField] Text playerName;
    [SerializeField] Text scoreValue;


    public void ChangeScoreText(string text)
    {
        scoreValue.text = text;
    }
    public void DisplayPlayerName(string text)
    {
        playerName.text = text;

    }

}
    
}

