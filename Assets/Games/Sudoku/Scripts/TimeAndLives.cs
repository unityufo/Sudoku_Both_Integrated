using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

namespace UFO.Games.sudoku
{
    public class TimeAndLives : MonoBehaviour   // this class implements timer and health system and notification related to it
{
    
    public List<GameObject> error_images = new List<GameObject>();
    [SerializeField] bool useNumber;

    static bool Gameover=false;
    int lives =0;

 [SerializeField] Text lifes;
 [SerializeField] Text finalScoreText;
    [SerializeField] int max_LIfe; 

    [SerializeField] float time_minutes;
    [SerializeField] float time_seconds;

    public Text timer;
     [SerializeField] Text gameOver_Message;

    float maxtimeinseconds;
    float currentTimeinSeconds;
    
    int error_number=0;



     public Color colorIni = Color.white;
     public Color colorFin = Color.red;
     public float duration = 3f;
     Color lerpedColor = Color.white;
 
     private float t = 0;
     private bool flag;
 
    public  Image _renderer;

    bool game_over;

    bool startTimerinsync = false; 

    [SerializeField] GameObject waitingForPlayerFinish;


  
        void Start()
        {
            // lives = error_images.Count;
            InitialiseTimeSettings();
        }

        private void InitialiseTimeSettings()
        {
            error_number = 0;

            var test = GameSettings.Instance as GameSettings;
            if (test != null)
                maxtimeinseconds = (GameSettings.Instance.GetGameTimeMinutes() * 60) + GameSettings.Instance.GetGameTimeseconds();
            else maxtimeinseconds = 180f;

            lifes.text = " x " + max_LIfe.ToString();
            currentTimeinSeconds = maxtimeinseconds;
        }

        bool canStartGame = false; 


        private void Update() {
        if(!canStartGame) return;
        
        if(game_over) return;
        
        if(currentTimeinSeconds <=0 ){ GameOver(); }    // GameoverMethod(); gameOver_Message.text = "Time Over"; return;}


       if(!game_over) currentTimeinSeconds -= Time.deltaTime;
        if(currentTimeinSeconds <=0)print("game over");
        int min = Mathf.FloorToInt(currentTimeinSeconds / 60);
        int sec = Mathf.FloorToInt(currentTimeinSeconds % 60);
        min = Mathf.Clamp(min,0,min);
        sec = Mathf.Clamp(sec,0,sec);
         timer.GetComponent<UnityEngine.UI.Text>().text = min.ToString("00") + ":" + sec.ToString("00");

        //  if(Input.GetKey(KeyCode.A))
        //  {
        //      lerpedColor = Color.Lerp(colorIni, colorFin, t);
        //  _renderer.color = lerpedColor;
 
        //  if (flag == true)
        //  {
        //      t -= Time.deltaTime / duration;
        //      if (t < 0.01f)
        //          flag = false;
        //  }
        //  else
        //  {
        //      t += Time.deltaTime / duration;
        //      if (t > 0.99f)
        //          flag = true;
        //  }
        //  }
        
    }

    private void WrongNumber()
    {
        if(!useNumber)
        {
            if(error_number < error_images.Count)
            {
            error_images[lives-1].SetActive(false);
            error_number++;
            lives--;
            Handheld.Vibrate();
            
             }

        }
        else 
        {
            if(max_LIfe > 0 ){ max_LIfe--; lifes.text = " x " +  max_LIfe.ToString();    }
            if(max_LIfe <= 0){ lifes.text =  " x " + max_LIfe.ToString(); GameOver();}  //GameoverMethod(); if(gameOver_Message!=null) gameOver_Message.text = "No lives left!!";}
        }    

    }


    [SerializeField] GameObject gameOverPanel;
    // public void GameoverMethod()
    // {
    //     game_over=true;
    //     GameEvents.OnGameOverMethod(true);
    //     gameOverPanel.SetActive(true);


       
    //    MakeAllButtonsNonInteractable();

    // }

    ////temp methods later change for better 

    void MakeAllButtonsNonInteractable()
    {
         foreach (var squareGrid in SudokuGrid.getSudokuGrid() )
        {
            squareGrid.GetComponent<GridSquare>().interactable = false;
        }

        foreach (var item in FindObjectsOfType<NumberInputButton>())
        {
            item.interactable = false;
        }

        foreach (var item in FindObjectsOfType<NoteButton>())
        {
            item.GetComponent<Button>().interactable=false;
            
        }

    }


        void SetGameOVerBool(bool inputbool)
        {
            game_over = inputbool;
            SetWaitingForPlayerScreen();
        }

        public GameObject gameWonScreen;
        [SerializeField] Text gameWin_Message;
        


        void SetGameWon(bool inputBool)
        {
            if(inputBool)
            {
                    gameWonScreen.SetActive(true);
                    float tempTime = maxtimeinseconds-currentTimeinSeconds;
                    int min = Mathf.FloorToInt(tempTime / 60);
                    int sec = Mathf.FloorToInt(tempTime % 60);
                    min = Mathf.Clamp(min,0,min);
                    sec = Mathf.Clamp(sec,0,sec);
                    //timer.GetComponent<UnityEngine.UI.Text>().text = min.ToString("00") + ":" + sec.ToString("00");
                    gameWin_Message.text = "You Cracked  the  Game in      " + min.ToString("00") + "   :   " + sec.ToString("00");
                   
            }
        }

    


    void OnEnable() {
         GameEvents.onGameStart +=    ()=>{canStartGame = true; };
        GameEvents.onWrongNumber += WrongNumber;
         GameEvents.onGameOver += SetGameOVerBool;
      //  GameEvents.onGameWin += SetGameOVerBool;
         GameEvents.onGameWin += SetGameWon;
         GameEvents.onGameLost += GameLost;
    }
     void OnDisable() {
        GameEvents.onWrongNumber -= WrongNumber;
        GameEvents.onGameOver -= SetGameOVerBool;
      
      ///  GameEvents.onGameWin -= SetGameOVerBool;
       GameEvents.onGameWin -= SetGameWon;
       GameEvents.onGameLost -= GameLost;
    }

/////////////////////////////////////Multiplayer methods //////temp /////////////


void GameOver()
{
    game_over = true;
    ScoreHolder.playerproperties["GameOver"] = true;
    PhotonNetwork.SetPlayerCustomProperties(ScoreHolder.playerproperties);
    GameEvents.OnGameOverMethod(true);
}

public static void setGameOver()
{
     ScoreHolder.playerproperties["GameOver"] = true;
    PhotonNetwork.SetPlayerCustomProperties(ScoreHolder.playerproperties);
}

void SetWaitingForPlayerScreen()
{
    waitingForPlayerFinish.SetActive(true);

}
   public void GameLost(bool value)
    {
        if(value == false) return;
        
        gameOverPanel.SetActive(true);
        gameOver_Message.text = "You lost"; return;


       
      

    }

     


    
}




    
}
