using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using  Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.Networking;
using SimpleJSON;

using UFO.Games.sudoku;


namespace  UFO.GlobalComponents
{
    public class MatchmakingSetup : MonoBehaviourPunCallbacks
{
   ////////////////////TestStuff
    
   //[SerializeField] GameObject mask;

   /////////////////////////
 
    [SerializeField] GamePropertiesList _gamePropertiesList;

    [Header("Matchmaking panel")]
   [SerializeField] GameObject matchmaking_Panel;
   [SerializeField] Text TitleMessageMM;
   [SerializeField] List<Text> playerListText = new List<Text>();

   [SerializeField] Text NetworkDebugText;

   [SerializeField] Button StartGameButton;



    ///////test
   [SerializeField] GameObject localPlayerProfile;


    void SetLocalPlayerProfile(string _playerName, Sprite profilepic ) //remove null later cuz hard to debug
   {
    
      Image playerProfilePic = localPlayerProfile.transform.GetChild(0).GetComponent<Image>();
      Text playerNameText = localPlayerProfile.transform.GetChild(1).GetComponent<Text>();

      playerNameText.text = _playerName;
      Debug.Log(playerProfilePic.sprite);
      playerProfilePic.sprite = profilepic;

   }
   
   public void SetLocalPlayerProfile()
   {
        SetLocalPlayerProfile(UFO_TEMP_GAME_SETTINGS.UserName,GlobalGameManager._instance.PlayerProfile.sprite);
   }

    ///////////////

    [SerializeField] float nonMasterClientWaitTime = 15;
    bool startMatchMaking = false;
    
    public float matchMakingWaitTime = 10;
    float Timer;
    bool hasEnoughplayerinRoom = false;
    bool isJoiningRoom;
    bool isCreatingRoom;
    
    bool sceneloadRequestsent=false; // just a bool ato avoid calling loadscene multiple times
   
    float Timer2;



    





   
    //    void Awake()
    // {
    //     StartGameButton.interactable = false;
    //     PhotonNetwork.ConnectUsingSettings();
    //     NetworkDebugText.text = "Cant Start game, Not connected to server";
    // }

    private void Start() 
    {
        Timer=matchMakingWaitTime;
        Timer2 = nonMasterClientWaitTime;

        if(Timer2 < Timer)
        {
            Debug.LogError("timer2 less than timer  ...can cause issues");
        }
       
       
    }
   
    






     public void forceConnectPhoton()
    {
        if(PhotonNetwork.IsConnected) { if(!istryingJoiningLobby) TryJoinLobby();   return; }
        string token = UFO_TEMP_GAME_SETTINGS.Token;

        if(string.IsNullOrEmpty(token )) { Debug.Log("token is not present , login in again to get the token");  Debug.LogError("notoken"); return;  }

        TryConnectToPhoton(token);

        Debug.LogError("forceConnectCalled");

     
    }

    private void TryConnectToPhoton(string token)
    {
        if(string.IsNullOrEmpty(token)) {  Debug.LogError("token authentication isEmpty"); return; }

        AuthenticationValues authValues = new AuthenticationValues();
         authValues.AuthType = CustomAuthenticationType.Custom;
       

            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("user_id", "1"); // replace with player's user_id
            dict.Add("token", token); // replace with player's Authentication token
            authValues.UserId = "1"; // replace with player's user_id
            authValues.SetAuthPostData(dict);


        PhotonNetwork.AuthValues = authValues;

        PhotonNetwork.ConnectUsingSettings();

        PhotonNetwork.AutomaticallySyncScene = false;

        

    }


    

    ////////////////////////////////
  

    
    

    // IEnumerator StartMatchmakingPhotonWait()
    // {
        
    // }

    Hashtable GetCustomGameProperties(string Game)
    {  
        Hashtable hash=  new Hashtable(); 
        hash.Add("hh0","fromfuction1");
        hash.Add("hhl","fromfuction2");
        hash.Add("hh2","fromfuction3");    
       
        return  hash;
    }

    [SerializeField]public Hashtable table;

    [SerializeField]List<stringstring> listt = new System.Collections.Generic.List<stringstring>();
    [System.Serializable]
    public struct stringstring
    {
      [SerializeField]  public string key;
       [SerializeField] public string val;

    }

    private void Update()  // remove these functions from update and use callbacks .....!
    {
        // if(Input.GetKeyDown(KeyCode.Space ))
        // {
        //        Hashtable nn = new Hashtable();
        //        nn.Add("intenal0", "internal val 1");
        //        nn.Add("intenal1", "internal val 2");
        //        nn.Add("intenal2", "internal val 2");

        //         nn.Merge(GetCustomGameProperties(""));
        //     foreach (var item in nn)
        //     {
        //         Debug.Log(nn[item] + " : " + item.Value);
        //     }


        // }




       if(!startMatchMaking) return;
       
       
        if(hasEnoughplayerinRoom  && PhotonNetwork.InRoom)// when max player fills
        {
            if(PhotonNetwork.IsMasterClient  && !sceneloadRequestsent)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false; 
                //LoadSyncLevel();
                this.photonView.RPC("LoadSyncLevel",RpcTarget.AllViaServer);
                sceneloadRequestsent=true;
                return;
            }
            //for non-masterclient ...extra call required???
                
        }
       
        if(PhotonNetwork.IsMasterClient && PhotonNetwork.InRoom) //timeout
        {
       
            if(startMatchMaking && !hasEnoughplayerinRoom && !sceneloadRequestsent)
            {
                if(Timer > 0.1f )
                {
                    Timer-= Time.deltaTime;
                    TitleMessageMM.text ="Finding other players...wait "+ Mathf.CeilToInt(Timer).ToString();
                    return;
                }
                if(Timer <=0.2)
                {
                   
                    sceneloadRequestsent=true;
                   // LoadASync();
                    this.photonView.RPC("LoadSyncLevel",RpcTarget.AllBufferedViaServer);
                    Debug.Log("Synced level");
                   
                    return;
                }

                ///other stuffs?
         
            }

        }
        else if(!PhotonNetwork.IsMasterClient  && !sceneloadRequestsent)
        {

             if(Timer2 > 0.1f )
            {
                TitleMessageMM.text ="Finding other players...wait "+ Mathf.CeilToInt(Timer2).ToString();
                Timer2-= Time.deltaTime;
                return;
            }
            if(Timer2 <=0.2)
            {
                   
                sceneloadRequestsent=true;
                LoadASync();
                Debug.Log("Synced level");
                return;
                   
                  
            }



        }
    }

    bool HasEnoughPlayerInRoom()
    {
        if(PhotonNetwork.PlayerList.Length == UFO_TEMP_GAME_SETTINGS.MaxPlayers) return true;
        return false;

    }
    [SerializeField] bool automaticallystart = true;
    public  override void OnConnectedToMaster()
    {
        TryJoinLobby();

    }
    bool istryingJoiningLobby = false;
    private void TryJoinLobby()
    {
        istryingJoiningLobby = true;
        PhotonNetwork.JoinLobby(Photon.Realtime.TypedLobby.Default);
        StartGameButton.interactable = true;
        NetworkDebugText.text = "Connected to Server";
        NetworkDebugText.color = Color.green;
        Debug.Log("Connected");
        Debug.Log(PhotonNetwork.NickName);

        if (automaticallystart)
        {
            StartCoroutine(TryGetRoomID());

        }
    }



    IEnumerator TryGetRoomID()
    {
        string url = "https://ufogames.webc.in/api/ufo/enter-match";
        WWWForm form = new WWWForm();
        form.AddField("match_table_template_id",UFO_TEMP_GAME_SETTINGS.Template_id);
           
        
         using (UnityWebRequest www = UnityWebRequest.Post(url,form))
        {
            www.SetRequestHeader("Accept","application/json");
            www.SetRequestHeader("Authorization","Bearer " + UFO_TEMP_GAME_SETTINGS.Token);

            var i = www.SendWebRequest();
      
            while(!i.isDone)
            {       

                Debug.Log("Waiting :  addTime maybe?" );
               
                 yield return null;
            }
            if (www.isNetworkError || www.isHttpError)
            {
                    Debug.Log(www.downloadHandler.text);
                    

            }

            
                string  recievedData = www.downloadHandler.text;

                Debug.Log(recievedData);
                JSONNode jsondata = JSONNode.Parse(recievedData);

                ///
                
               // PlayerPrefs.SetString("logintestdata")

                ///
                string Photon_game_id = jsondata["data"]["photon_game_id"]; 

              
                 
                Debug.Log ("is roomID empty ? :  " +string.IsNullOrEmpty(Photon_game_id)) ;

                if(string.IsNullOrEmpty(Photon_game_id)) //is null
                {
                    CreateRoom();
                }
                else
                {
                    Debug.Log("Join room by ID");
                    UFO_TEMP_GAME_SETTINGS.Photon_game_id = Photon_game_id;
                    PhotonNetwork.JoinRoom(Photon_game_id);


                }

                
        }
    }

 

    //join Random////////////////////////////
    public void JoinRandom() //new
    {
        byte maxPlayers = (byte)UFO_TEMP_GAME_SETTINGS.MaxPlayers;

        Hashtable expectedCustomRoomProperties = new Hashtable();

        expectedCustomRoomProperties.Add("template_id", UFO_TEMP_GAME_SETTINGS.Template_id);
    
        PhotonNetwork.JoinRandomRoom(expectedCustomRoomProperties,
        maxPlayers, MatchmakingMode.FillRoom, null, null, null);
    }



    public override void OnJoinRandomFailed(short returnCode, string message) //new
    {
        Debug.Log("Join random faliled" + message);
        CreateRoom();
    }
    

/////////////////////////////////////////////////////////////////


    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log(returnCode + " : " + message);
        CreateRoom();
    }


////////////////////////////////////////////////////////////////
    private void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        byte maxPlayers = (byte)UFO_TEMP_GAME_SETTINGS.MaxPlayers;
        roomOptions.MaxPlayers = maxPlayers;
        roomOptions.PublishUserId = true;
        // PlayerTtl needs to be -1 for Async games
        roomOptions.PlayerTtl =-1;
        // 5000 milliseconds
        roomOptions.EmptyRoomTtl = 5000;
        #if SERVERSDK
        roomOptions.CheckUserOnJoin = true;
        #endif
        roomOptions.CustomRoomProperties = new Hashtable();
        roomOptions.CustomRoomProperties.Add("template_id", UFO_TEMP_GAME_SETTINGS.Template_id);
        
        string[] customPropsForLobby = { "template_id" };
        roomOptions.CustomRoomPropertiesForLobby = customPropsForLobby;
        PhotonNetwork.CreateRoom(null, roomOptions, null, null);

        Debug.Log("Room created");
    }


    




     public override void OnErrorInfo(ErrorInfo errorInfo)
    {
        Debug.Log(errorInfo.ToString());
    }




    // public void CreateGame()
    // {
        
    //     PhotonNetwork.CreateRoom(CreateGameInput.text,new RoomOptions(){ MaxPlayers = 5, EmptyRoomTtl = 300000 ,PlayerTtl =  300000  },null); //86400000 = 24 hrs
    // }
    // public void JoinGame()
    // {
    //     RoomOptions roomOptions =  new RoomOptions();
    //     roomOptions.MaxPlayers = 5;
    //     roomOptions.PlayerTtl=  300000 ;
       
    //    // PhotonNetwork.JoinOrCreateRoom(JoinGameInput.text,roomOptions,TypedLobby.Default);
    //    PhotonNetwork.JoinRoom(JoinGameInput.text);


    // }




    
     
    

    public void JoinRandomRoom()
    {   
        byte maxPlayers = (byte)UFO_TEMP_GAME_SETTINGS.MaxPlayers;
        GameSettings.Instance.SetIsREalTime(true);
        PhotonNetwork.JoinRandomRoom(null,maxPlayers);
        isJoiningRoom = true;
        MatchmakingFuctions();
        //PhotonNetwork.JoinRandomOrCreateRoom(null,maxPlayers,roomOptions :  new RoomOptions{MaxPlayers = maxPlayers},typedLobby: TypedLobby.Default);
    }

////////////////////////////////////////////////////////////////////////////////

    int randomlistValue;
    int randomSudokuDataValue;
    // public override void OnJoinRandomFailed(short returnCode, string message) //old
    // {       
            
    //         Debug.Log("No rooms found creating a room");
    //         isJoiningRoom = false;
    //         TitleMessageMM.text = "No Rooms Found ..Creating Room";

    //         //creating random indexes for choosing sudoku data//now doing from gameSetting///cahnge in later Iteration
    //         /////////////////////////////////////////////////////////////
           
    //         GameSettings.Instance.SetCorrectDataset(GameSettings.Instance.GetDIfficulty());
    //         Hashtable roomproperties = new Hashtable();
    //         roomproperties.Add("LI",GameSettings.Instance.GetListIndexValue());
    //         roomproperties.Add("SI",GameSettings.Instance.GetSudokuDataIndexValue());
           
    //        ////////////////////////////////////////////////////////////


    //         RoomOptions _roomOptions =  new RoomOptions();
    //         _roomOptions.MaxPlayers = maxPlayers;
    //         _roomOptions.CustomRoomProperties = roomproperties;
           
    //         PhotonNetwork.CreateRoom(null,roomOptions: _roomOptions,typedLobby: TypedLobby.Default);
    //         isCreatingRoom = true;
    // }
   

    // public override void OnRoomListUpdate(List<RoomInfo> roomList)
    // {
    //         Debug.Log("Roomlistt Update Called");
    // }
    

////////////Temp functions///needs refactoring/////////////////////
        void MatchmakingFuctions()
        {

            // if(!PhotonNetwork.InRoom && isJoiningRoom && !startMatchMaking)
            // {
            //     matchmaking_Panel.SetActive(true);
            //     TitleMessageMM.text = " Joining Room ... ";
             
               
            // }
            // else if(!PhotonNetwork.InRoom && isCreatingRoom && !startMatchMaking)
            // {
            //         TitleMessageMM.text = "Creating  Room ... ";
            // }


            if(PhotonNetwork.InRoom && startMatchMaking && !PhotonNetwork.IsMasterClient)
            {
                if(!matchmaking_Panel.activeInHierarchy)matchmaking_Panel.SetActive(true);
                   TitleMessageMM.text = "Waiting for Host to Start Game";

             
            }
             else if(PhotonNetwork.InRoom && startMatchMaking && PhotonNetwork.IsMasterClient)
            {
                if(!matchmaking_Panel.activeInHierarchy)matchmaking_Panel.SetActive(true);
                TitleMessageMM.text = "Waiting for Other Players";

             
            }
        }


    void ClearPlayerListField()
    {
        foreach (var playerText in playerListText)
        {
            playerText.text = "";
            
        }
    }

    void setPlayerList()
    {
        int i =0;
        var otherplayer = PhotonNetwork.PlayerListOthers;
        
        if(otherplayer.Length == 0 || otherplayer == null) return;
        
        foreach (Text playerText in playerListText)
        {
        
            if(i < otherplayer.Length) //i/0 logic!!!
            {
                playerText.text = otherplayer[i].NickName;
                i++;          
            }
            if(i > otherplayer.Length) return;
            
            
        }
    }




///////////////////////////////////////////////////////

public override void OnRoomListUpdate(List<RoomInfo> roomList)
{
    Debug.Log("A room was found ?");
    foreach (var item in roomList)
    {
      
        Debug.Log(item.Name);
        
    }
}


    
    [SerializeField] Text  roomIDTExt;

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
       // joiningRoom.SetActive(false);
       // startMatchMaking = true;

        ClearPlayerListField();
        setPlayerList();

        Debug.Log("new player Joined");

        hasEnoughplayerinRoom = HasEnoughPlayerInRoom();
            //  if(hasEnoughplayerinRoom  && PhotonNetwork.InRoom)
            //    {
            //       if(PhotonNetwork.IsMasterClient  && !sceneloadRequestsent)
            //       {
            //         //PhotonNetwork.CurrentRoom.IsOpen = false; 
            //         LoadSyncLevel();
            //         sceneloadRequestsent=true;
            //         return;
            //       }
                
            //     }
        
    }
   public override void OnPlayerLeftRoom(	Player 	otherPlayer	)	
     {
         hasEnoughplayerinRoom = HasEnoughPlayerInRoom();
           MatchmakingFuctions();
          ClearPlayerListField();
          setPlayerList();
  
     }

   
   
   
   
    public   override  void  OnJoinedRoom()
    {
        UFO_TEMP_GAME_SETTINGS.Photon_game_id = PhotonNetwork.CurrentRoom.Name;

     
       
           ////////////////////////// 
    
            PhotonNetwork.LocalPlayer.CustomProperties.Add("ProfilePicLink",UFO_TEMP_GAME_SETTINGS.ProfilePictureLink);
            /////////////////////////////

         TitleMessageMM.text = " Room Joined ";
         startMatchMaking = true;
         MatchmakingFuctions();
         ClearPlayerListField();
         setPlayerList();
          hasEnoughplayerinRoom = HasEnoughPlayerInRoom();

        roomIDTExt.text = "Room ID : " +  PhotonNetwork.CurrentRoom.Name;
       
        Debug.Log(PhotonNetwork.CurrentRoom.Name);
        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        // foreach (var item in PhotonNetwork.PlayerListOthers)
        // {
        //      Debug.Log(item.NickName);
        // }
        
//         messageTet.text = "Room joined" +  PhotonNetwork.CurrentRoom.Name;
        //PhotonNetwork.LoadLevel("game");
    }


    
    [PunRPC]
    public  void LoadSyncLevel() 
    {
        int gameScene = GlobalGameManager._instance.GetSelectedGame().GetSceneIndex(); 
        PhotonNetwork.LoadLevel(gameScene);
 
    }
    public  void LoadASync()
    {
        int gameScene = GlobalGameManager._instance.GetSelectedGame().GetSceneIndex(); 
        PhotonNetwork.LoadLevel(gameScene);
    }

    

}

}



