using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace UFO.Games.sudoku
{
    public class PlayerScorePrefab : MonoBehaviour
{
    [SerializeField] Text PlayerName, PlayerStatus, Score; 
    [SerializeField] Image PlayerProfile,OnlineStatusIndicator;

    [SerializeField] Sprite errorSprite; ////change to resource based loading >>>>>>>later 

    public void SetPlayerData(PlayerScoreData data)
    {
        PlayerName.text  =  data.PlayerName;
        PlayerStatus.text = data.OnlineStatus.ToString();
        Score.text = data.Score.ToString();
        if(!string.IsNullOrEmpty(data.ProfilePicLink)) SetProfilePicFromWEb(data.ProfilePicLink);
    }

    public void UpdateStatus(Onlinestatus _status)
    {
        PlayerStatus.text = _status.ToString(); 
        if(_status == Onlinestatus.Online){ OnlineStatusIndicator.color = Color.green; }
        else { OnlineStatusIndicator.color = Color.red; }
    }

    public void SetProfilePicFromWEb(string url)
    {
        StartCoroutine(GetPlayerImageFromWEb(url));
    }

    public void UpdateScore(int score)
    {
        Score.text = score.ToString();
    }
    IEnumerator GetPlayerImageFromWEb(string url)//change the flow structure....weird shit
    {
    
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if(www.isHttpError)
        {
        PlayerProfile.sprite = errorSprite;
        }

        Texture2D myTexture = DownloadHandlerTexture.GetContent(www);

        Rect rec = new Rect(0, 0, myTexture.width, myTexture.height);
        Sprite downloadedSprite = Sprite.Create(myTexture, rec, new Vector2(0.5f, 0.5f), 100);

        PlayerProfile.sprite = downloadedSprite;   
         
    }


}

public class PlayerScoreData
{
    public string PlayerName;
    public Onlinestatus OnlineStatus;

    public int Score;

    public string ProfilePicLink;

    public PlayerScoreData(string _playerName,int _score)
    {
        PlayerName = _playerName;
        Score = _score;
    }

    

}
public enum Onlinestatus
{
        Online,Offline
}
}
