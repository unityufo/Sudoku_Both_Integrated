using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace UFO.Games.sudoku
{

    public class GridSquare : Selectable, IPointerClickHandler, ISubmitHandler, IPointerExitHandler
    {

        //#if DEVELOPMENT_BUILD

        /////developer options_ function  /// only for test built

        bool test = true;
        bool DeveloperOptionOn = false;
        public bool isDeveloperOptionOn { get => DeveloperOptionOn; set { DeveloperOptionOn = value; } }
        public void HideText()
        {
            number_text.GetComponent<Text>().text = "";
        }

        public void ChangeTextColour(Color colourvalue)
        {
            number_text.GetComponent<Text>().color = colourvalue;

        }

        public void ShowValue()
        {
            number_text.GetComponent<Text>().text = correct_number.ToString();

        }


        ///////////////////////////////////////////

        [SerializeField] GameObject number_text;

        public List<GameObject> number_notes;

        public List<GameObject> fruit_sprites = new List<GameObject>();
        private bool note_active;

        bool lineHighted = false;
        public bool isLineHighlighted { get { return lineHighted; } set { lineHighted = value; } }



        Animator anim;

        [SerializeField] private int number = 0;
        [SerializeField] private int correct_number = 0;

        //public int get_number_value{get {return number; }}

        public int getgridvalue() { return number; }
        [SerializeField] Color wrongIDColour;


        private bool has_default_value = false;

        public void SetHasDefaultValue(bool has_Default) { has_default_value = has_Default; }
        public bool GetHasDefaultValue() { return has_default_value; }


        bool has_WrongNumber = false;

        public void SetHasWrongNumber(bool set_has_wrong)
        {
            has_WrongNumber = set_has_wrong;
        }
        public bool GetHasWrongNumber()
        {
            return has_WrongNumber;
        }






        private bool selected = false;

        private int square_Index = -1;
        public bool IsSelected() { return selected; }




        public void SetCorrectNumber(int _number)
        {
            correct_number = _number;
        }


        public void SetSquareIndex(int index)
        {
            square_Index = index;
        }



        private new void Start()
        {
            selected = false;
            note_active = false;
            //SetGridNotes(number_notes);
            // SetNoteNumberValue(0);
            SetClearEmptyNotes();
            anim = GetComponent<Animator>();
        }

        // public List<string> GetSquareNotes()
        // {
        //     List<String> notes = new List<string>();

        //     foreach (var number in number_notes)
        //     {
        //         notes.Add(number.GetComponent<Text>().text);

        //     }
        //     return notes;
        // }

        private void SetClearEmptyNotes()
        {
            foreach (var numbers in number_notes)
            {
                //if(number.GetComponent<Text>().text == "0") number.GetComponent<Text>().text = ""; 
                // SetActive(false);

            }
        }

        private void SetNoteNumberValue(int value)
        {
            foreach (var number in number_notes)
            {
                if (value <= 0) number.SetActive(false);    // number.GetComponent<Text>().text = "";
                else
                    number.SetActive(true); // number.GetComponent<Text>().text = value.ToString();
            }
        }

        public void SetNoteSingleNumberValue(int value, bool force_update = false)
        {
            if (note_active == false && force_update == false) return;

            if (value <= 0) number_notes[value - 1].SetActive(false);//GetComponent<Text>().text  = "";
            else
            {
                if (number_notes[value - 1].gameObject.activeInHierarchy == false || force_update)
                {
                    number_notes[value - 1].gameObject.SetActive(true);//GetComponent<Text>().text = value.ToString();
                }
                else
                {
                    number_notes[value - 1].gameObject.SetActive(false);//GetComponent<Text>().text = "";
                }

            }

        }


        public void SetGridNotes(List<int> notes)
        {
            foreach (var note in notes)
            {

                SetNoteSingleNumberValue(note, true);
            }
        }

        public void OnNoteActive(bool active)
        {
            note_active = active;
        }





        public void DisplayText()
        {
            if (number <= 0) number_text.GetComponent<Text>().text = "";
            ////////////////////////////
            else number_text.GetComponent<Text>().text = number.ToString();

            if (DeveloperOptionOn) { DeveloperOptionOn = false; ChangeTextColour(Color.black); }
        }



        public void SetNumber(int _number)
        {
            // Debug.Log("number set" + _number);
            number = _number;
            DisplayText();


        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //    if(GameSettings.Instance.toggleGridSelection)
            //    {
            selected = true;
            GameEvents.currentGrid = square_Index;
            //         if(selected)changeColour(Color.blue);
            //    }

            GameEvents.SquareSelectedMethod(square_Index);
        }

        // private void changeColour(Color colorvalue)
        // {
        //     this.colors.pressedColor = new Color(colorvalue.a,colorvalue.b,colorvalue.g);
        // }

        // void checkDuplicateEntry(int squareIndex)
        // {
        //     if(square_Index != squareIndex)
        //     {
        //         selected =false;
        //         //print(squareIndex);
        //     }

        // }

        public void OnSubmit(BaseEventData eventData)
        {
            // throw new NotImplementedException();
        }
        private void OnSquareSelected(int squareIndex)
        {
            if (square_Index != squareIndex)
            {
                selected = false;
                //print(squareIndex);
            }
            //else  if(square_Index == squareIndex){selected=!selected; print(squareIndex);}
        }

        public void SetSquareColor(Color col)
        {
            var colors = this.colors;
            colors.normalColor = col;
            this.colors = colors;
        }

        private new void OnEnable()
        {
            GameEvents.onUpdateSquareNumber += OnSetNumber;
            GameEvents.onUpdateSquareNumber += OnSquareSelected;
            GameEvents.OnNotesActive += OnNoteActive;
        }


        private new void OnDisable()
        {
            GameEvents.onUpdateSquareNumber -= OnSetNumber;
            GameEvents.onUpdateSquareNumber -= OnSquareSelected;
            GameEvents.OnNotesActive -= OnNoteActive;
        }

        public void OnSetNumber(int _number)
        {
            // if(note_active && !has_default_value){ SetNoteSingleNumberValue(number);  return;}


            if (selected && GameEvents.currentGrid == square_Index && !has_default_value)
            {
                // if(note_active ==true && !has_default_value){ SetNoteSingleNumberValue(_number,true);}



                if (note_active == false)
                {
                    SetNoteNumberValue(0);
                    SetNumber(_number);
                    // SetNoteNumberValue(0);
                    if (number != correct_number)
                    {
                        has_WrongNumber = true;
                        var colors = this.colors;
                        colors.normalColor = wrongIDColour;
                        this.colors = colors;

                        GameEvents.OnWrongNumberMethod();
                        SetHasWrongNumber(true); // change this later 
                        SetNoteNumberValue(0);
                    }
                    else
                    {
                        has_WrongNumber = false;
                        has_default_value = true;
                        var colors = this.colors;
                        colors.normalColor = Color.white;
                        this.colors = colors;
                        GameEvents.OnGetPointMethod(5);  /// change this later
                        GameEvents.OnRightAnswerMethod(square_Index);

                        SetNoteNumberValue(0);

                    }
                }
            }

        }

        public int getSquareIndex()
        {
            return square_Index;
        }

        public void StartSweepAnimation(float time, string direction)
        {

            StartCoroutine(SweepColor(time, direction));
        }

        IEnumerator SweepColor(float time, string direction)
        {
            yield return new WaitForSeconds(time);
            anim.SetTrigger(direction);

        }
    }
}
