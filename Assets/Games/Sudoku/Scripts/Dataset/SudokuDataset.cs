﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[CreateAssetMenu(fileName = "SudokuDataset", menuName = "Sudoku_Test/Create Dataset", order = 0)]

public class SudokuDataset : GamePropertiesProcessor
{
    [SerializeField]  Difficulty Difficulty_Level;
    [SerializeField]   List<SudokuData> sudokuDataLIst = new List<SudokuData>();

    public int getIndex{ get => sudokuDataLIst.Count;}

    public override object GetData()
    {
        int maxRange = sudokuDataLIst.Count -1;
        int random = Random.Range(0,maxRange);

        return random;
    }

  

    public Difficulty GetDifficulty()
{
    return Difficulty_Level;
}
    public SudokuData GetSudokuData(int  index)  // implement this later
    {
        return  sudokuDataLIst[index];
    }

    //  public SudokuData getSolvedData(int  index)  // implement this later
    // {
    //     return  sudokuDataLIst[index];
    // }

}

public enum Difficulty
{
    easy,medium,hard
}


[System.Serializable]

    
 public class SudokuData 

{
    public string unsolvedData;

    public string solvedData;


}
