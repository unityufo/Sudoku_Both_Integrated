﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "GameRoomProperties", menuName = "UFO/Game/GameRoomProperties", order = 0)]
public class GameRoomProperties : ScriptableObject {
    
    [System.Serializable]
    struct CustomKeyVal //cuz unity cant ::editor serialise:: Dictionary
    {
        public string key;
       public GamePropertiesProcessor val;
    }


   [SerializeField] List<CustomKeyVal> GameProperties = new List<CustomKeyVal>();

    [SerializeReference] [SerializeField]List<IgameProperty> property = new List<IgameProperty>();
    public Dictionary<string,object> GetRoomProperties()
    {
        Dictionary<string,object> newDict = new Dictionary<string, object>();

        foreach (var item in GameProperties)
        {
            newDict[item.key] = item.val.GetData();
        }

        return newDict;
    }
}


public abstract class GamePropertiesProcessor: ScriptableObject
{
   public abstract   object GetData();
    
}


public interface IgameProperty
{

}

public class gtx: IgameProperty
{

}