﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "GamePropertiesList", menuName = "UFO/Game/GamePropertiesList", order = 0)]
public class GamePropertiesList : ScriptableObject {

    [System.Serializable]
    struct CustomKeyVal
    {

        public string GameKey;
        public GameRoomProperties properties;


    }

    [Tooltip("Make sure only one item of a gamekey exist, only the first element of a particular key is used ,any duplicate will be ignored")]
    [SerializeField] List<CustomKeyVal> GameRoomProperties;



    public GameRoomProperties GetGameProperties(string _gameKey)
    {
        GameRoomProperties _gameProp  = null ;

        foreach (var item in GameRoomProperties)
        {
              if(item.GameKey == _gameKey) 
              {
                 _gameProp = item.properties;
              }
              else
              {
                Debug.LogError("Game Key Not Found");  
              } 
        }

         return _gameProp; 
    }

    
}