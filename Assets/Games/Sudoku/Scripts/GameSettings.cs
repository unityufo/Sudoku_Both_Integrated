using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace UFO.Games.sudoku
{


 

    public class GameSettings : MonoBehaviour //singleton instance 
{
  
   
 


  
public static GameSettings Instance ;

 public  Dictionary<string,PlayerStats> playerStats = new Dictionary<string, PlayerStats>();



//[SerializeField] List<SudokuDataset> _sudoku_datasetList = new List<SudokuDataset>();

static SudokuData  CurrentDataSet;
////game setting ///developer Options //////////////////////////////////////////////////
 

bool isRealtimeGame = false;
public void  SetIsREalTime(bool val)
{
    isRealtimeGame = val;
}
public bool GetIsRealTime()
{
    return isRealtimeGame;
}

 

    int maxplayer=2; ///the maxplayer for the current game 
    public void  SetmaxPlayer(int _max_player)
    {
            maxplayer = _max_player;
    }
    public int maxPlayerPossible {get => maxplayer;}

 public static SudokuData GetDataset()
 {
     if(CurrentDataSet == null)
     {
         Debug.LogError("data is not curently set/is null. returning  a default dataset "); 
        SudokuData newData = new SudokuData();
        newData.unsolvedData = "000060280709001000860320074900040510007190340003006002002970000300800905500000021";
        newData.solvedData =   "431567289729481653865329174986243517257198346143756892612975438374812965598634721";
        return newData;
    }
     //_sudoku_datasetList[0].GetSudokuData(0);} //developement helper 
     return CurrentDataSet;
 }
 
 
 float game_time_minutes = 3f;
 float game_time_seconds = 0f;


public void SetGameTime(float minutes=0,float seconds = 30)
{
    game_time_minutes = minutes;
    game_time_seconds = seconds;

}

public float  GetGameTimeMinutes()
{
    return game_time_minutes;
}
public float  GetGameTimeseconds()
{
    return game_time_seconds;
}

bool isInvincible=false;

public void Set_invincibility(bool invinciblity)
{
    isInvincible = invinciblity;
}
public bool GetIsInvincible()
{
    return isInvincible;
}
 
 

/////////////////////////////////////////////////////////////////////////////////////////



//public Button StartGameButton;
 Difficulty difficultyLevel;
    private bool useRandom = true;

//     private void Awake() {
//     if(Instance == null)
//     {
//       DontDestroyOnLoad(this);
//        Instance = this;
//     }
//    else Destroy(this);
// }






public Difficulty GetDIfficulty()
{
    return difficultyLevel;
}


 


int randomlistValue =0;
int randomSudokuDataValue=0;
public int  GetListIndexValue()
{
    return randomlistValue; 
}
public int GetSudokuDataIndexValue()
{
    return randomSudokuDataValue;
}

    public void SetCorrectDataset(Difficulty _game_difficulty)  // will not work with multiplayer because of encapsulation has to try random in diffrent way later 
    {   
        List<SudokuDataset> filteredDataSet  = new List<SudokuDataset>();
        foreach (var datalist in GameDataHolder.GetSudokuDatasets())
        {
            if(datalist.GetDifficulty() == _game_difficulty)
            {
                filteredDataSet.Add(datalist);
            }
            
        }

        if(useRandom) //just a modified version of singleplayer function for multiplayer/better solution will be implemented in the next iteration
        {
            randomlistValue = Random.Range(0,filteredDataSet.Count - 1);
            Debug.Log("list    : " + randomlistValue);

            randomSudokuDataValue = Random.Range(0,filteredDataSet[randomlistValue].getIndex );

            Debug.Log("data :  " + randomSudokuDataValue);

            CurrentDataSet = filteredDataSet[randomlistValue].GetSudokuData(randomSudokuDataValue);
          

            
        }
    }

    public void SetMultiplayerSudokuData(int listValue,int sudokuDataValue)
    {
        List<SudokuDataset> filteredDataSet  = new List<SudokuDataset>();
        foreach (var datalist in GameDataHolder.GetSudokuDatasets())
        {
                filteredDataSet.Add(datalist);
           
        }
        CurrentDataSet = filteredDataSet[listValue].GetSudokuData(sudokuDataValue);
    }


    public void DestroyThis()
    {
        Destroy(this);

    }

    public void StartGame()
    {
        // SetCorrectDataset(difficultyLevel);
         if(CurrentDataSet==null) Debug.LogError("Current Data is empty");
         print("GameSetting ::::  " + CurrentDataSet);
         //isRealtimeGame=false;
       //  MatchmakingSetup.LoadSyncLevel();
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

       
       
    }

    
}
    
}
