﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
namespace UFO.Games.sudoku
{
    public class NoteButton : MonoBehaviour //Selectable, IPointerClickHandler, ISubmitHandler, IPointerExitHandler
{
    public Sprite noteSwitch_on;
    public Sprite noteSwitch_off;

    bool active=false;
    Button noteButton;

   // private Color button_defaultColor;

 private void Start() {
     noteButton = GetComponent<Button>();
     noteButton.onClick.AddListener(()=> noteModeActivator()) ;
 }

// public void OnPointerClick(PointerEventData eventData)
//     {
//         noteModeActivator();
//     }

    private void noteModeActivator()
    {
        active = !active;
        if (active)
        {
            GameEvents.OnNotesActiveMethod(true);
            GetComponent<Image>().sprite = noteSwitch_on;


            GameEvents.isNoteMode = true;
            return;

        }

        if (!active)
        {
            GameEvents.OnNotesActiveMethod(false); GameEvents.isNoteMode = false; GetComponent<Image>().sprite = noteSwitch_off;
        }
    }

    public void OnSubmit(BaseEventData eventData)
    {
       
    }
}


}
