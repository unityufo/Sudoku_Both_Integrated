﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


namespace UFO.Games.sudoku
{
    public class Game_initManager_normal : MonoBehaviourPunCallbacks
    {

        GameObject introFadeSceneOBject;
    
        bool req_CustomProp_Recieved = false;

        void  Start()
        {
            
                  
                // GameSettings.Instance.SetCorrectDataset(Difficulty.easy);
                // GameSettings.Instance.SetMultiplayerSudokuData(0,0);
                StartCoroutine(RunIntro());
                
                return;
            


            if (PhotonNetwork.IsMasterClient)
            {
                //create random value and set index
                PhotonNetwork.CurrentRoom.CustomProperties.Add("QI", GameSettings.GetDataset());
                //run intro....
            }
            else
            {
                if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("QI"))
                {
                    //set that index
                }
                
                //checkcanstartGame
                //runintro
            }

            ConfirmEntryandDeductBalance();
        }


        public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
        {
           if(!req_CustomProp_Recieved)
           {
             if(propertiesThatChanged.ContainsKey("QI"))
             {

                req_CustomProp_Recieved = true;
             }
           }
        }



        IEnumerator CheckIfCanStartGame()
        {
            float timer = 0;
            float check_interval = 0.2f;
            while(!req_CustomProp_Recieved)
            {
                timer+= Time.deltaTime;
                if(timer > check_interval)
                {
                    timer = 0;
                    if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("QI"))
                    {
                        //Set that index
                        req_CustomProp_Recieved = true;
                    }
                }
                yield return null;
            }
        } 

       [SerializeField] StartGameIntro gameintroObject;

        IEnumerator RunIntro()
        {
           yield return gameintroObject.StartCountDown();
            
            Debug.Log("startcalled");

            SetStartGame();
            gameintroObject.gameObject.SetActive(false);
            yield return null;

        }





        void ConfirmEntryandDeductBalance()
        {

        }



        void SetStartGame()
        {
            GameEvents.CanGameStartMethod();
            Debug.Log("startcalled");
        }       




    }



}